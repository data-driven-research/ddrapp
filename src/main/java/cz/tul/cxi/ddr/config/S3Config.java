package cz.tul.cxi.ddr.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;

import java.net.URI;

@Configuration
public class S3Config {
    @Value("${s3.endpoint_url}")
    private String endpointURL;

    @Value("${s3.vo_name}")
    private String voName;

    @Value("${s3.access_key}")
    private String access_key;

    @Value("${s3.secret_key}")
    private String secret_key;

    @Bean
    public S3Client cesnetS3Client() {
        return S3Client.builder()
                .credentialsProvider(cesnetCredentialsProvider())
                .endpointOverride(URI.create(endpointURL))
                .region(Region.AWS_GLOBAL)
                .serviceConfiguration(S3Configuration.builder()
                        .pathStyleAccessEnabled(true)
                        .build())
                .build();
    }

    @Bean
    public String s3EndpointURL() {
        return String.format("%s/%s", endpointURL, voName);
    }

    private AwsCredentialsProvider cesnetCredentialsProvider() {
        return StaticCredentialsProvider
                .create(AwsBasicCredentials.create(access_key, secret_key));
    }
}
