package cz.tul.cxi.ddr.controller;


import cz.tul.cxi.ddr.model.postgres.Dataset;
import cz.tul.cxi.ddr.model.postgres.Instrument;
import cz.tul.cxi.ddr.model.postgres.Measurement;
import cz.tul.cxi.ddr.model.postgres.CompositeMeasurementId;
import cz.tul.cxi.ddr.service.DatasetService;
import cz.tul.cxi.ddr.service.InstrumentService;
import cz.tul.cxi.ddr.service.MeasurementService;
import cz.tul.cxi.ddr.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.UUID;

@Controller
@RequestMapping("/easy")
public class EasyController {

    private final DatasetService datasetService;

    private final InstrumentService instrumentService;

    private final MeasurementService measurementService;

    private final UserService userService;

    public EasyController(DatasetService datasetService,
                          InstrumentService instrumentService,
                          MeasurementService measurementService,
                          UserService userService) {
        this.datasetService = datasetService;
        this.instrumentService = instrumentService;
        this.measurementService = measurementService;
        this.userService = userService;
    }

    @GetMapping(value = { "", "/" })
    public String index(Authentication authentication) {
        return String.format(
                "redirect:/easy/list/measurements?byEntity=user&byId=%s&byAcronym=%s&activeOnly=true",
                authentication.getName(),
                authentication.getName());
    }

    @GetMapping("/registration/measurement")
    public String getMeasurementForm(@RequestParam String byEntity,
                                     @RequestParam String byId,
                                     @RequestParam String byAcronym,
                                     Authentication authentication,
                                     RedirectAttributes redirectAttributes,
                                     Model model,
                                     ArrayList<Instrument> loadedInstruments,
                                     ArrayList<Dataset> loadedDatasets) {
        if (model.containsAttribute("measurement")) {
            return "easy/measurement";
        }

        // kdyz chybi instrument nebo dataset
        loadedInstruments = instrumentService.findInstrumentsByUserAndAvailability(
                authentication.getName(), true);
        if (loadedInstruments.isEmpty()) {
            redirectAttributes.addFlashAttribute(
                    "warning",
                    "Přejděte na modernější počítač a zkontrolujte dostupnost přístroje."
            );
            return String.format(
                    "redirect:/easy/list/measurements?byEntity=user&byId=%s&byAcronym=%s&activeOnly=true",
                    authentication.getName(),
                    authentication.getName());
        }

        loadedDatasets = datasetService.findDatasetsByUserAndActivity(authentication.getName(), true);
        if (loadedDatasets.isEmpty()) {
            redirectAttributes.addFlashAttribute(
                    "warning",
                    "Přejděte na modernější počítač a zkontrolujte dostupné datasety."
            );
            return String.format(
                    "redirect:/easy/list/measurements?byEntity=user&byId=%s&byAcronym=%s&activeOnly=true",
                    authentication.getName(),
                    authentication.getName());
        }

        model.addAttribute("measurement",
                measurementService.createDummyMeasurement(byEntity, byId, byAcronym));
        model.addAttribute("loadedInstruments", loadedInstruments);
        model.addAttribute("loadedDatasets", loadedDatasets);

        return "easy/measurement";
    }

    @PostMapping("/registration/measurement")
    public String postMeasurement(Authentication authentication,
                                  @ModelAttribute Measurement measurement,
                                  RedirectAttributes redirectAttributes,
                                  Measurement startedMeasurement) {
        if (measurementService.measurementExistsByNameAndDataset(measurement)) {
            redirectAttributes.addFlashAttribute(
                    "warning",
                    String.format(
                            "Měření %s už v datasetu %s existuje.",
                            measurement.getAcronym(),
                            measurement.getDataset().getAcronym()
                    ));
            return String.format(
                    "redirect:/easy/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                    measurement.getDataset().getId(),
                    measurement.getDataset().getAcronym());
        }

        startedMeasurement = measurementService.startMeasurement(
                measurement,
                userService.findById(authentication.getName()).orElseThrow(),
                false);
        instrumentService.setMeasuring(measurement.getInstrument().getId(), true);
        measurementService.triggerPreparation(startedMeasurement);

        return String.format(
                "redirect:/easy/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                measurement.getDataset().getId(),
                measurement.getDataset().getAcronym());
    }

    @GetMapping("/list/measurements")
    @PreAuthorize("!#byEntity.equals('admin') or hasRole('ADMINISTRATOR')")
    public String getMeasurementsList(@RequestParam String byEntity,
                                      @RequestParam String byId,
                                      @RequestParam String byAcronym,
                                      @RequestParam Boolean activeOnly,
                                      Model model) {
        model.addAttribute("loadedMeasurements", measurementService
                .findMeasurementsByEntityAndActivity(byEntity, byId, activeOnly));
        model.addAttribute("byEntity", byEntity);
        model.addAttribute("byId", byId);
        model.addAttribute("byAcronym", byAcronym);
        model.addAttribute("showAll", !activeOnly);

        return "easy/measurements";
    }

    @GetMapping("/action/measurement/{measurementId}/{repetition}/end")
    public String getEndMeasurementForm(@PathVariable UUID measurementId,
                                        @PathVariable int repetition,
                                        Authentication authentication,
                                        Model model) {
        Measurement measurement = measurementService.findMeasurementByFullIdAndUserEmail(
                new CompositeMeasurementId(measurementId, repetition), authentication.getName());
        measurement.setEnd(Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague")))));
        model.addAttribute("endingMeasurement", measurement);
        return "easy/measurement-end";
    }

    @PostMapping(value = "/action/measurement/end", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String endMeasurement(Authentication authentication,
                                 @ModelAttribute Measurement endingMeasurement,
                                 Measurement measurement) {
        measurement = measurementService.updateByEndingMeasurement(endingMeasurement);
        measurementService.endMeasurement(measurement, userService.findById(authentication.getName()).orElseThrow());
        instrumentService.setMeasuring(measurement.getInstrument().getId(), false);
        measurementService.triggerBackup(measurement);

        return String.format(
                "redirect:/easy/registration/measurement?byEntity=user&byId=%s&byAcronym=%s",
                authentication.getName(),
                authentication.getName());
    }

    @GetMapping("/action/measurement/{measurementId}/{repetition}/repeat")
    public String repeatMeasurement(@PathVariable("measurementId") UUID measurementId,
                                    @PathVariable("repetition") int repetition,
                                    Authentication authentication,
                                    RedirectAttributes redirectAttributes,
                                    Measurement measurement) {
        measurement = measurementService.findMeasurementByFullIdAndUserEmail(
                new CompositeMeasurementId(measurementId, repetition), authentication.getName());
        if (!instrumentService.isInstrumentAvailableOfUser(measurement.getInstrument().getId(), authentication.getName())) {
            redirectAttributes.addFlashAttribute("warning",
                    String.format("Přístroj %s použitý pro měření %s není dostupný.",
                            measurement.getInstrument().getAcronym(), measurement.getAcronym()));
            return String.format(
                    "redirect:/easy/list/instruments?byEntity=user&byId=%s&byAcronym=%s&availableOnly=false",
                    authentication.getName(),
                    authentication.getName()
            );
        }
        if (!datasetService.datasetIsAvailable(measurement.getDataset().getId(), authentication.getName())) {
            redirectAttributes.addFlashAttribute("warning",
                    String.format("Dataset %s použitý pro měření %s není dostupný.",
                            measurement.getDataset().getAcronym(), measurement.getAcronym()));
            return String.format(
                    "redirect:/easy/list/datasets?byEntity=user&byId=%s&byAcronym=%s&activeOnly=false",
                    authentication.getName(),
                    authentication.getName()
            );
        }
        measurementService.startMeasurement(
                measurement,
                userService.findById(authentication.getName()).orElseThrow(),
                true);
        instrumentService.setMeasuring(measurement.getInstrument().getId(), true);

        return String.format(
                "redirect:/easy/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                measurement.getDataset().getId(),
                measurement.getDataset().getAcronym());
    }
}
