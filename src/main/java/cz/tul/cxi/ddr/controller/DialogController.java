package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.model.postgres.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cz.tul.cxi.ddr.service.InstrumentService;
import cz.tul.cxi.ddr.service.UserService;

@Controller
@RequestMapping("/dialog")
public class DialogController {

    private final UserService userService;
    private final InstrumentService instrumentService;

    public DialogController(UserService userService, InstrumentService instrumentService) {
        this.userService = userService;
        this.instrumentService = instrumentService;
    }

    @GetMapping(value = "/dlg-user/{id}")
    public String getUserDetail(@PathVariable String id, Model model) {
        model.addAttribute("userProfile", userService.findById(id).orElseThrow());
        return "dialog/dlg-user";
    }

    @GetMapping(value = "/dlg-instrument/{id}")
    public String getInstrumentDetail(@PathVariable String id, Model model) {
        model.addAttribute("instrument", instrumentService.findById(id).orElseThrow());
        return "dialog/dlg-instrument";
    }

    @GetMapping(value = "/dlg-zip")
    public String downloadZip(Model model) {
        return "dialog/dlg-zip";
    }

    @GetMapping(value = "/dlg-sharepub")
    public String sharePublic(Model model) {
        return "dialog/dlg-sharepub";
    }
}
