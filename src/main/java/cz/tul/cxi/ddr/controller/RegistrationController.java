package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.model.postgres.Dataset;
import cz.tul.cxi.ddr.model.postgres.Instrument;
import cz.tul.cxi.ddr.model.postgres.Measurement;
import cz.tul.cxi.ddr.model.postgres.Resource;
import cz.tul.cxi.ddr.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.ArrayList;

@Controller
@RequestMapping("/registration")
@Slf4j
public class RegistrationController {
    private final ResourceService resourceService;

    private final DatasetService datasetService;

    private final InstrumentService instrumentService;

    private final MeasurementService measurementService;

    private final UserService userService;

    private final S3CoreService s3c;

    public RegistrationController(ResourceService resourceService,
                                  DatasetService datasetService,
                                  InstrumentService instrumentService,
                                  MeasurementService measurementService,
                                  UserService userService,
                                  S3CoreService s3c) {
        this.resourceService = resourceService;
        this.datasetService = datasetService;
        this.instrumentService = instrumentService;
        this.measurementService = measurementService;
        this.userService = userService;
        this.s3c = s3c;
    }

    @GetMapping("/resource")
    public String getResourceForm(Model model) {
        if (!model.containsAttribute("resource")) {
            model.addAttribute("resource", new Resource());
        }
        return "registration/resource";
    }

    @PostMapping(value = "/resource/loadFromGap", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String loadGaPResource(@RequestParam String internalNumber,
                                  @RequestParam String activity,
                                  RedirectAttributes redirectAttributes,
                                  Model model) {
        Resource[] loadedResources = resourceService.loadGaPResources(internalNumber, activity);
        if (loadedResources == null || loadedResources[0] == null) {
            redirectAttributes.addFlashAttribute("warning",
                    "Žádný zdroj nebyl nalezen.");
            return "redirect:/registration/resource";
        }
        model.addAttribute("resource", loadedResources[0]);
        if (loadedResources.length > 1) {
            model.addAttribute("loadedResources", loadedResources);
        }
        return "registration/resource";
    }

    @PostMapping(value = "/resource", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postResource(@ModelAttribute Resource resource,
                               Model model) {
        if (resourceService.resourceExists(resource)) {
            model.addAttribute("warning", "Daný zdroj už existuje.");
            model.addAttribute("resource", resource);
            return "registration/resource";
        }
        resource = resourceService.save(resource);
        String bucketName = resource.getDataPath().replace("/", "");
        s3c.createBucket(bucketName);

        return "redirect:/registration/dataset?resourceId=" + resource.getId();
    }

    @GetMapping("/dataset")
    public String getDatasetForm(@RequestParam(required = false) Long resourceId,
                                 Model model) {
        if (!model.containsAttribute("dataset")) {
            Dataset dataset = new Dataset();
            if (resourceId != null) {
                dataset.setResource(resourceService.findById(resourceId).orElse(new Resource()));
            }
            model.addAttribute("dataset", dataset);
            model.addAttribute("loadedResources", resourceService.findAll());
        }
        return "registration/dataset";
    }

    @PostMapping(value = "/dataset", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postDataset(Authentication authentication,
                              @ModelAttribute Dataset dataset,
                              Model model) {
        if (datasetService.datasetExistsByNamesAndResource(dataset)) {
            model.addAttribute("warning",
                    "Dataset se zadaným názvem či akronymem už v rámci vybraného zdroje existuje.");
            model.addAttribute("dataset", dataset);
            model.addAttribute("loadedResources", resourceService.findAll());
            return "registration/dataset";
        }
        log.debug("User: " + authentication);
        dataset = datasetService.saveDataset(userService.findById(authentication.getName()).orElseThrow(), dataset);
        s3c.createDataset(dataset, userService.findById(authentication.getName()).orElseThrow());

        return String.format(
                "redirect:/list/datasets?byEntity=user&byId=%s&byAcronym=%s&activeOnly=true",
                authentication.getName(),
                authentication.getName()
        );
    }

    @GetMapping("/instrument")
    public String getInstrumentForm(Model model) {
        if (!model.containsAttribute("instrument")) {
            model.addAttribute("instrument", new Instrument());
        }
        return "registration/instrument";
    }

    @PostMapping(value = "/instrument", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String postInstrument(Authentication authentication,
                                 @ModelAttribute Instrument instrument,
                                 @RequestParam("image") MultipartFile image,
                                 Model model) throws IOException {
        if (instrumentService.instrumentExists(instrument)) {
            model.addAttribute("warning",
                    "Přístroj s daným inventárním číslem už existuje.");
            model.addAttribute("instrument", instrument);
            return "registration/instrument";
        }
        if (instrumentService.instrumentExistsByNames(instrument)) {
            model.addAttribute("warning",
                    "Přístroj s daným názvem či akronymem už existuje.");
            model.addAttribute("instrument", instrument);
            return "registration/instrument";
        }
        if (!instrumentService.isConnectible(instrument)) {
            model.addAttribute("warning", "Tento přístroj není připojitelný z " +
                    "jednoho z následujících důvodů: \n 1. Je to real-time zařízení.");
            return "registration/instrument";
        }
        log.debug("User: " + authentication);
        String imageName = instrumentService.saveImage(instrument, image);
        instrumentService.saveInstrument(userService.findById(authentication.getName()).orElseThrow(), instrument, imageName);
        instrumentService.sendInstrumentInstallationRequest(userService.findById(authentication.getName()).orElseThrow(), instrument);

        return "registration/instrument-registered";
    }

    @GetMapping("/measurement")
    public String getMeasurementForm(Authentication authentication,
                                     @RequestParam String byEntity,
                                     @RequestParam String byId,
                                     @RequestParam String byAcronym,
                                     RedirectAttributes redirectAttributes,
                                     Model model,
                                     ArrayList<Instrument> loadedInstruments,
                                     ArrayList<Dataset> loadedDatasets) {
        if (model.containsAttribute("measurement")) {
            return "registration/measurement";
        }
        log.debug("User: " + authentication);
        loadedInstruments = instrumentService.findInstrumentsByUserAndAvailability(
                authentication.getName(), true);
        if (loadedInstruments.isEmpty()) {
            redirectAttributes.addFlashAttribute(
                    "warning",
                    "Nenalezeny žádné volné přístroje."
            );
            return "redirect:/list/instruments";
        }

        loadedDatasets = datasetService.findDatasetsByUserAndActivity(authentication.getName(), true);
        if (loadedDatasets.isEmpty()) {
            redirectAttributes.addFlashAttribute(
                    "warning",
                    "Nenalezeny žádné aktivní datasety."
            );
            return String.format(
                    "redirect:/list/datasets?byEntity=user&byId=%s&byAcronym=%s&activeOnly=true",
                    authentication.getName(),
                    authentication.getName());
        }

        model.addAttribute("measurement",
                measurementService.createDummyMeasurement(byEntity, byId, byAcronym));
        model.addAttribute("loadedInstruments", loadedInstruments);
        model.addAttribute("loadedDatasets", loadedDatasets);

        return "registration/measurement";
    }

    @PostMapping("/measurement")
    public String postMeasurement(Authentication authentication,
                                  @ModelAttribute Measurement measurement,
                                  RedirectAttributes redirectAttributes,
                                  Measurement startedMeasurement) {
        if (measurementService.measurementExistsByNameAndDataset(measurement)) {
            redirectAttributes.addFlashAttribute(
                    "warning",
                    String.format(
                            "Měření %s už v datasetu %s existuje.",
                            measurement.getAcronym(),
                            measurement.getDataset().getAcronym()
                    ));
            return String.format(
                    "redirect:/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                    measurement.getDataset().getId(),
                    measurement.getDataset().getAcronym());
        }
        log.debug("User: " + authentication);
        startedMeasurement = measurementService.startMeasurement(
                measurement,
                userService.findById(authentication.getName()).orElseThrow(),
                false);
        instrumentService.setMeasuring(measurement.getInstrument().getId(), true);
        measurementService.triggerPreparation(startedMeasurement);

        return String.format(
                "redirect:/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                measurement.getDataset().getId(),
                measurement.getDataset().getAcronym());
    }
}
