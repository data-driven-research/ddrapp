package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.model.postgres.User;
import cz.tul.cxi.ddr.service.DatasetService;
import cz.tul.cxi.ddr.service.InstrumentService;
import cz.tul.cxi.ddr.service.S3CoreService;
import cz.tul.cxi.ddr.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.UUID;

@Controller
@RequestMapping("/modification")
@Slf4j
public class ModificationController {

    private final UserService userService;

    private final InstrumentService instrumentService;

    private final DatasetService datasetService;

    private final S3CoreService s3c;

    public ModificationController(UserService userService,
                                  InstrumentService instrumentService,
                                  DatasetService datasetService,
                                  S3CoreService s3c) {
        this.userService = userService;
        this.instrumentService = instrumentService;
        this.datasetService = datasetService;
        this.s3c = s3c;
    }

    @GetMapping("/instrument/{instrumentId}/setInstalled/{installed}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String setInstrumentInstalled(Authentication authentication,
                                         @PathVariable("instrumentId") String instrumentId,
                                         @PathVariable("installed") Boolean installed,
                                         RedirectAttributes redirectAttributes) {
        instrumentService.setInstalled(instrumentId, installed);
        String message = installed ? "Přístroj úspěšně aktivován." : "Přístroj úspěšně deaktivován.";
        redirectAttributes.addFlashAttribute("success", message);

        return "redirect:/list/instruments";
    }

    @GetMapping("/{entity}/{entityId}/addUser")
    public String getAddUserToEntityForm(@PathVariable String entity,
                                         @PathVariable String entityId,
                                         @RequestParam String entityAcronym,
                                         Model model) {
        model.addAttribute("loadedUsers", userService.findUserNotPresentInEntity(entity, entityId));
        model.addAttribute("entity", entity);
        model.addAttribute("entityId", entityId);
        model.addAttribute("entityAcronym", entityAcronym);
        model.addAttribute("userToAdd", new User());

        return "modification/user-add";
    }

    @PostMapping(value = "/{entity}/{entityId}/addUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postAddUserToEntity(@ModelAttribute User userToAdd,
                                      @PathVariable String entity,
                                      @PathVariable String entityId,
                                      @RequestParam String entityAcronym,
                                      RedirectAttributes redirectAttributes) {
        switch (entity) {
            case "instrument" -> instrumentService.addUserToInstrument(userToAdd.getEmail(), entityId);
            case "dataset" -> {
                datasetService.addUserToDataset(userToAdd.getEmail(), UUID.fromString(entityId));
                s3c.addUserToDataset(
                        userService.findById(userToAdd.getEmail()).orElseThrow(),
                        datasetService.findById(UUID.fromString(entityId)).orElseThrow()
                );
            }
            case "s3dataset" -> {
                String dataPath = entityId.replace("+", "/");
                datasetService.addUserToS3Dataset(userToAdd.getEmail(), dataPath);
                s3c.addUserToDataset(
                        userService.findById(userToAdd.getEmail()).orElseThrow(),
                        datasetService.findById(UUID.fromString(entityId)).orElseThrow()
                );
            }
            default -> throw new IllegalArgumentException(
                    "Cannot add user to given " + entity + ".");
        }
        redirectAttributes.addFlashAttribute("success", "Uživatel úspěšně přidán.");

        return String.format(
                "redirect:/list/users?byEntity=%s&byId=%s&byAcronym=%s&%s=true",
                entity,
                entityId,
                entityAcronym,
                entity.equals("instrument") ? "availableOnly" : "activeOnly");
    }

    @GetMapping(value = "/{entity}/{entityId}/removeUser/{userEmail}")
    public String getRemoveUserFromEntity(@PathVariable String entity,
                                          @PathVariable String entityId,
                                          @PathVariable String userEmail,
                                          @RequestParam String entityAcronym,
                                          RedirectAttributes redirectAttributes) {
        switch (entity) {
            case "instrument" -> instrumentService.removeUserFromInstrument(userEmail, entityId);
            default -> throw new IllegalArgumentException(
                    "Cannot remove user from given " + entity + ".");
        }
        redirectAttributes.addFlashAttribute("success", "Uživatel úspěšně odebrán.");
        return String.format(
                "redirect:/list/users?byEntity=%s&byId=%s&byAcronym=%s&%s=true",
                entity,
                entityId,
                entityAcronym,
                entity.equals("instrument") ? "availableOnly" : "activeOnly");
    }

    @PostMapping(value = "/user/updateProfile", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String requestProfileChange(@ModelAttribute User userProfile, RedirectAttributes redirectAttributes) {
        userService.save(userProfile);
        redirectAttributes.addFlashAttribute("success", "user.saved.successfully");
        return String.format("redirect:/detail/user/%s", userProfile.getEmail());
    }

    @PostMapping(value = "/user/updateProfileRequest", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String requestProfileChange(Authentication authentication,
                                       @ModelAttribute User userProfile,
                                       RedirectAttributes redirectAttributes) {
        userService.sendProfileChangeRequest(userProfile, userService.findById(authentication.getName()).orElseThrow());
        redirectAttributes.addFlashAttribute("success", "Údaje byly odeslány ke změně.");
        return String.format("redirect:/detail/user/%s", userProfile.getEmail());
    }
}
