package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.service.DatasetService;
import cz.tul.cxi.ddr.service.MeasurementService;
import cz.tul.cxi.ddr.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Slf4j
@RequestMapping("/list")
public class ListController {

    private final DatasetService datasetService;
    private final MeasurementService measurementService;
    private final UserService userService;

    public ListController(DatasetService datasetService,
                          MeasurementService measurementService,
                          UserService userService) {
        this.datasetService = datasetService;
        this.measurementService = measurementService;
        this.userService = userService;
    }

    @GetMapping("/instruments")
    public String getInstrumentsList() {
        return "list/instruments";
    }

    @GetMapping("/datasets")
    @PreAuthorize("!#byEntity.equals('admin') or hasRole('ADMINISTRATOR')")
    public String getDatasetsList(@RequestParam String byEntity, @RequestParam String byId, @RequestParam String byAcronym, @RequestParam Boolean activeOnly, Model model) {
        model.addAttribute("loadedDatasets", datasetService.findDatasetsByEntityAndActivity(byEntity, byId, activeOnly));
        model.addAttribute("byEntity", byEntity);
        model.addAttribute("byId", byId);
        model.addAttribute("byAcronym", byAcronym);
        model.addAttribute("showAll", !activeOnly);

        return "list/datasets";
    }

    @GetMapping("/datasets2")
    public String getDatasetsList2() {
        return "list/datasets2";
    }

    @GetMapping("/measurements")
    @PreAuthorize("!#byEntity.equals('admin') or hasRole('ADMINISTRATOR')")
    public String getMeasurementsList(@RequestParam String byEntity, @RequestParam String byId, @RequestParam String byAcronym, @RequestParam Boolean activeOnly, Model model) {
        model.addAttribute("loadedMeasurements", measurementService.findMeasurementsByEntityAndActivity(byEntity, byId, activeOnly));
        model.addAttribute("byEntity", byEntity);
        model.addAttribute("byId", byId);
        model.addAttribute("byAcronym", byAcronym);
        model.addAttribute("showAll", !activeOnly);

        return "list/measurements";
    }

    @GetMapping("/measurements2")
    public String getMeasurementsList2() {
        return "list/measurements2";
    }

    @GetMapping("/reservations")
    public String getReservations() {
        return "list/reservations";
    }

    @GetMapping("/s3points")
    public String getS3points() {
        return "list/s3points";
    }

    @GetMapping("/users")
    @PreAuthorize("!#byEntity.equals('admin') or hasRole('ADMINISTRATOR')")
    public String getUsersList(@RequestParam String byEntity, @RequestParam String byId, @RequestParam String byAcronym, Model model) {
        model.addAttribute("loadedUsers", userService.findUsersByEntity(byEntity, byId));
        model.addAttribute("byEntity", byEntity);
        model.addAttribute("byAcronym", byAcronym);
        model.addAttribute("byId", byId);

        return "list/users";
    }

    @GetMapping("/alerts")
    public String getAlerts() {
        return "list/alerts";
    }

}
