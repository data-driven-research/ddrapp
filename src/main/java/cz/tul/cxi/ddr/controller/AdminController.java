package cz.tul.cxi.ddr.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.saml2.provider.service.authentication.Saml2AuthenticatedPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @GetMapping("/attributes")
    public String getAttributes(@AuthenticationPrincipal Saml2AuthenticatedPrincipal principal,
                                Authentication authentication) {
        StringBuilder result = new StringBuilder("<h3>Details<h3>\n");
        result.append("Name: ")
                .append(principal.getName()).append("<br/>\n");
        result.append("SP entityID: ")
                .append(principal.getRelyingPartyRegistrationId())
                .append("<br/>\n");

        result.append("Attributes: <br/>\n");
        for (String attName : principal.getAttributes().keySet()) {
            result.append(attName)
                    .append(": ")
                    .append(principal.getAttributes().get(attName))
                    .append("<br/>\n");
        }
        result.append("Authorities: <br/>\n");
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            result.append(authority)
                    .append("<br/>\n");
        }
        return result.toString();
    }

    @GetMapping("/user-info")
    public String getUserInfo(Authentication authentication, Principal principal) {
        return authentication.toString() + principal.toString();
    }
}
