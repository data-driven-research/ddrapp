package cz.tul.cxi.ddr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {
    @GetMapping("/")
    public String getConsole() {
        return "console";
    }

    @GetMapping("/not-implemented")
    public String notImplemented() {
        return "not-implemented";
    }

    @GetMapping("/login")
    String login() {
        return "login";
    }
}