package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/detail")
public class DetailController {

    private final UserService userService;

    public DetailController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/user/{email}")
    public String getUserDetail(@PathVariable String email, Model model) {
        model.addAttribute("userProfile", userService.findById(email).orElseThrow());
        return "detail/user";
    }
}
