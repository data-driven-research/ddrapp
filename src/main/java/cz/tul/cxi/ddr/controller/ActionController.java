package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.model.postgres.Instrument;
import cz.tul.cxi.ddr.model.postgres.Measurement;
import cz.tul.cxi.ddr.model.postgres.CompositeMeasurementId;
import cz.tul.cxi.ddr.model.s3.S3Object;
import cz.tul.cxi.ddr.service.*;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.UUID;

@Controller
@RequestMapping("/action")
@Slf4j
public class ActionController {

    private final InstrumentService instrumentService;

    private final DatasetService datasetService;

    private final MeasurementService measurementService;

    private final UserService userService;

    private final S3CoreService s3CoreService;

    public ActionController(InstrumentService instrumentService,
                            DatasetService datasetService,
                            MeasurementService measurementService,
                            UserService userService,
                            S3CoreService s3CoreService) {
        this.instrumentService = instrumentService;
        this.datasetService = datasetService;
        this.measurementService = measurementService;
        this.userService = userService;
        this.s3CoreService = s3CoreService;
    }

    @GetMapping("/instrument/{instrumentId}/downloadLogbook")
    public ResponseEntity<Resource> getInstrumentLog(Authentication authentication,
                                                     @PathVariable("instrumentId") String instrumentId,
                                                     Instrument instrument) {
        log.debug("User: " + authentication);
        instrument = instrumentService.findInstrumentByIdAndUserEmail(instrumentId, authentication.getName());
        String fileName = String.format(
                "%s-%s-%s.xlsx",
                instrument.getId(),
                instrument.getAcronym(),
                LocalDate.now());
        log.info("Downloading logbook " + fileName);
        Resource resource = instrumentService.downloadLog(fileName);

        if (resource.exists() && resource.isReadable()) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(
                    MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            headers.setContentDispositionFormData("attachment", fileName);
            return ResponseEntity.ok()
                    .headers(headers)
                    .body(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/dataset/{datasetId}/close")
    public String closeDataset(Authentication authentication,
                               @PathVariable UUID datasetId) {
        log.debug("User: " + authentication);
        datasetService.closeDataset(datasetId);

        return String.format(
                "redirect:/list/datasets?byEntity=user&byId=%s&byAcronym=%s&activeOnly=true",
                authentication.getName(),
                authentication.getName()
        );
    }

    @PostMapping(value = "/s3dataset/download", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> downloadS3Dataset(Authentication authentication,
                                                    @Valid @RequestBody S3Object s3Dataset) {
        log.debug("User: " + authentication + " downloading " + s3Dataset);

        String urls = s3CoreService.downloadS3Dataset(s3Dataset,
                userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>(urls, HttpStatus.OK);
    }

    @PostMapping(value = "/s3dataset/publicURL", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> publicShare(Authentication authentication,
                                                    @Valid @RequestBody S3Object s3Dataset) {
        log.debug("User: " + authentication + " sharing " + s3Dataset);

        String urls = s3CoreService.getPublicURLs(s3Dataset,
                userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>(urls, HttpStatus.OK);
    }


    @GetMapping("/measurement/{measurementId}/{repetition}/end")
    public String getEndMeasurementForm(Authentication authentication,
                                        @PathVariable UUID measurementId,
                                        @PathVariable int repetition,
                                        Model model,
                                        Measurement measurement) {
        log.debug("User: " + authentication);
        measurement = measurementService.findMeasurementByFullIdAndUserEmail(
                new CompositeMeasurementId(measurementId, repetition), authentication.getName());
        measurement.setEnd(Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague")))));
        model.addAttribute("endingMeasurement", measurement);
        model.addAttribute("datasetAcronym", measurement.getDataset().getAcronym());
        model.addAttribute("instrumentAcronym", measurement.getInstrument().getAcronym());

        return "action/measurement-end";
    }

    @PostMapping(value = "/measurement/end", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String endMeasurement(Authentication authentication,
                                 @ModelAttribute Measurement endingMeasurement,
                                 Measurement measurement) {
        log.debug("User: " + authentication);
        measurement = measurementService.updateByEndingMeasurement(endingMeasurement);
        measurementService.endMeasurement(measurement, userService.findById(authentication.getName()).orElseThrow());
        instrumentService.setMeasuring(measurement.getInstrument().getId(), false);
        measurementService.triggerBackup(measurement);

        return String.format(
                "redirect:/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                measurement.getDataset().getId(),
                measurement.getDataset().getAcronym());
    }

    @GetMapping("/measurement/{measurementId}/{repetition}/repeat")
    public String repeatMeasurement(Authentication authentication,
                                    @PathVariable("measurementId") UUID measurementId,
                                    @PathVariable("repetition") int repetition,
                                    RedirectAttributes redirectAttributes,
                                    Measurement measurement) {
        log.debug("User: " + authentication);
        measurement = measurementService.findMeasurementByFullIdAndUserEmail(
                new CompositeMeasurementId(measurementId, repetition), authentication.getName());
        if (!instrumentService.isInstrumentAvailableOfUser(measurement.getInstrument().getId(), authentication.getName())) {
            redirectAttributes.addFlashAttribute("warning",
                    String.format("Přístroj %s použitý pro měření %s není dostupný.",
                            measurement.getInstrument().getAcronym(), measurement.getAcronym()));
            return "redirect:/list/instruments";
        }
        if (!datasetService.datasetIsAvailable(measurement.getDataset().getId(), authentication.getName())) {
            redirectAttributes.addFlashAttribute("warning",
                    String.format("Dataset %s použitý pro měření %s není dostupný.",
                            measurement.getDataset().getAcronym(), measurement.getAcronym()));
            return String.format(
                    "redirect:/list/datasets?byEntity=user&byId=%s&byAcronym=%s&activeOnly=false",
                    authentication.getName(),
                    authentication.getName()
            );
        }
        measurementService.startMeasurement(
                measurement,
                userService.findById(authentication.getName()).orElseThrow(),
                true);
        instrumentService.setMeasuring(measurement.getInstrument().getId(), true);

        return String.format(
                "redirect:/list/measurements?byEntity=dataset&byId=%s&byAcronym=%s&activeOnly=true",
                measurement.getDataset().getId(),
                measurement.getDataset().getAcronym());
    }

    @PostMapping(value = "/s3object/download", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> downloadFile(Authentication authentication,
                                               @Valid @RequestBody S3Object s3Object) {
        log.debug("User: " + authentication + " downloading " + s3Object);

        String result = s3CoreService.downloadFile(s3Object, userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
