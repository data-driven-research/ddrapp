package cz.tul.cxi.ddr.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tul.cxi.ddr.model.s3.BucketPolicy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class JSONComponent {
    private final ResourceLoader resourceLoader;
    private final ObjectMapper objectMapper;

    public JSONComponent(ResourceLoader resourceLoader,
                         ObjectMapper objectMapper) {
        this.resourceLoader = resourceLoader;
        this.objectMapper = objectMapper;
    }

    public BucketPolicy readJsonFileToBucketPolicy(String filePath) throws IOException {
        // Load the JSON file from the classpath
        Resource resource = resourceLoader.getResource("classpath:" + filePath);

        // Use ObjectMapper to convert JSON to Map
        return objectMapper.readValue(resource.getInputStream(), BucketPolicy.class);
    }

    public String convertBucketPolicyToJsonString(BucketPolicy mapToConvert) throws JsonProcessingException {
        return objectMapper.writeValueAsString(mapToConvert);
    }

    public BucketPolicy convertJsonStringToBucketPolicy(String bucketPolicy) throws JsonProcessingException {
        return objectMapper.readValue(bucketPolicy, BucketPolicy.class);
    }
}
