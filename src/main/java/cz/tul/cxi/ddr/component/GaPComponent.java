package cz.tul.cxi.ddr.component;

import cz.tul.cxi.ddr.model.postgres.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GaPComponent {
    private final RestTemplate restTemplate;

    @Value("${gap.resources_url}")
    private String resourcesURL;

    public GaPComponent(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Resource[] loadResources(String internalNumber,
                                    String activity) {
        String uriPattern = resourcesURL + "?internalnumber=" + internalNumber;
        if (!activity.isEmpty()) {
            uriPattern += "&activity=" + activity;
            Resource chosenResource = restTemplate.getForObject(uriPattern, Resource.class);
            return new Resource[]{chosenResource};
        } else {
            return restTemplate.getForObject(uriPattern, Resource[].class);
        }
    }
}
