package cz.tul.cxi.ddr.component;

import cz.tul.cxi.ddr.model.s3.S3Bucket;
import cz.tul.cxi.ddr.model.s3.S3Object;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.IOException;

@Service
@Slf4j
public class S3CoreComponent {

    private final RestTemplate restTemplate;

    private final S3Client s3Client;

    private final String s3EndpointUrl;

    @Value("${s3core.url}")
    private String s3CoreUrl;

    public S3CoreComponent(RestTemplateBuilder builder,
                           S3Client s3Client,
                           String s3EndpointUrl) {
        this.restTemplate = builder.build();
        this.s3Client = s3Client;
        this.s3EndpointUrl = s3EndpointUrl;
    }

    public Resource getS3File(String bucketName,
                              String keyName){
        ResponseInputStream<GetObjectResponse> responseInputStream = s3Client.getObject(
                GetObjectRequest.builder().bucket(bucketName).key(keyName).build()
        );
        return new InputStreamResource(responseInputStream);

    }

    public void putS3File(String bucketName,
                          String keyName,
                          MultipartFile multipart) {
        PutObjectRequest objectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(keyName)
                .build();

        try {
            s3Client.putObject(
                    objectRequest,
                    RequestBody.fromInputStream(multipart.getInputStream(), multipart.getSize()));
        } catch (IOException e) {
            log.error("Error putting object to S3", e);
            throw new RuntimeException("Error putting object to S3", e);
        }
    }

    public String constructFullURL(String bucketName,
                                   String keyName) {
        return String.format("%s:%s/%s", s3EndpointUrl, bucketName, keyName);
    }

    public String listObjects(S3Object s3Dataset, String einfraId, boolean pure) {
        return restTemplate.postForObject(s3CoreUrl + "/objects?einfraId=" + einfraId + "&pure=" + pure, s3Dataset, String.class);
    }

    public String listDatasets(S3Bucket s3Bucket, String einfraId) {
        return restTemplate.postForObject(s3CoreUrl + "/datasets?einfraId=" + einfraId, s3Bucket, String.class);
    }

    public String listBuckets() {
        return restTemplate.getForObject(s3CoreUrl + "/buckets", String.class);
    }

    public String getPreSignedULRS(S3Object s3Dataset, String einfraId) {
        return restTemplate.postForObject(s3CoreUrl + "/objects/downloadUrls?einfraId=" + einfraId, s3Dataset, String.class);
    }

    public String downloadFile(S3Object s3Object, String einfraId) {
        return restTemplate.postForObject(s3CoreUrl + "/objects/download?einfraId=" + einfraId, s3Object, String.class);
    }

    public void shareDataset(S3Object s3Object, String einfraId) {
        restTemplate.put(s3CoreUrl + "/datasets/share?einfraId=" + einfraId, s3Object);
    }

    public void createBucket(S3Bucket s3Bucket) {
        restTemplate.put(s3CoreUrl + "/buckets", s3Bucket);
    }

    public void createDataset(S3Object s3Object, String einfraId) {
        restTemplate.put(s3CoreUrl + "/datasets?einfraId=" + einfraId, s3Object);
    }
}
