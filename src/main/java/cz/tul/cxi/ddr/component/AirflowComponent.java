package cz.tul.cxi.ddr.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AirflowComponent {
    private final RestTemplate restTemplate;

    @Value("${airflow.baseUrl}")
    private String baseUrl;

    @Value("${airflow.token}")
    private String token;

    public AirflowComponent(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void triggerDAG(String dagName,
                           String dagConf) {
        String url = baseUrl + "/dags/" + dagName + "/dagRuns";

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> httpEntity = new HttpEntity<>(dagConf, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                httpEntity,
                String.class);

        if (response.getStatusCode() != HttpStatusCode.valueOf(200)) {
            String message = String.format("Response code %s when triggering DAG %s with conf %s",
                    response.getStatusCode(), dagName, dagConf);
            throw new IllegalStateException(message);
        }
    }
}
