package cz.tul.cxi.ddr.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class EmailComponent {

    private final JavaMailSender mailSender;

    @Value("${admin.email.from}")
    private String adminEmailFrom;

    @Value("${admin.email.to}")
    private List<String> adminEmailTo;

    public EmailComponent(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendSimpleMessage(String subject,
                                  String text,
                                  String cc)
            throws MailException {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom(adminEmailFrom);
        msg.setTo(adminEmailTo.toArray(new String[0]));
        msg.setCc("ddr@tul.cz", cc);
        msg.setSubject(subject);
        msg.setText(text);

        this.mailSender.send(msg);
    }
}
