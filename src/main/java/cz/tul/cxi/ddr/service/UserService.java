package cz.tul.cxi.ddr.service;

import cz.tul.cxi.ddr.component.EmailComponent;
import cz.tul.cxi.ddr.model.postgres.User;
import cz.tul.cxi.ddr.repository.AlertRepository;
import cz.tul.cxi.ddr.repository.GenericRepository;
import cz.tul.cxi.ddr.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toCollection;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final AlertRepository alertRepository;
    private final EmailComponent emailComponent;

    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, AlertRepository alertRepository, EmailComponent emailComponent, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.alertRepository = alertRepository;
        this.emailComponent = emailComponent;
        this.passwordEncoder = passwordEncoder;
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void sendProfileChangeRequest(User userToChange,
                                         User requestingUser)
            throws MailException {
        String subject = "Approval of a DDRapp profile change";
        String text = String.format("User %s, Role: %s, EInfraID: %s,username: %s requests change of user %s to %s %s",
                requestingUser.getEmail(), requestingUser.getRole(), requestingUser.getEinfraId(),
                requestingUser.getUsername(), userToChange.getEmail(),
                userToChange.getUDepartmentAcronym(), userToChange.getRole());
        log.info(text);
        emailComponent.sendSimpleMessage(subject, text, requestingUser.getEmail());
    }

    public Optional<User> findById(String email) {
        return userRepository.findById(email);
    }

    public ArrayList<User> findUserNotPresentInEntity(String entity,
                                                      String entityId) {
        switch (entity) {
            case "dataset" -> {
                return findUsersNotPresentInDataset(UUID.fromString(entityId));
            }
            case "instrument" -> {
                return findUsersNotPresentInInstrument(entityId);
            }
            default -> throw new IllegalStateException(
                    "Wrong entity (" + entity + ") to add a user to.");
        }
    }

    private ArrayList<User> findUsersNotPresentInDataset(UUID datasetId) {
        return userRepository.findUsersNotPresentInDataset(datasetId)
                .stream().sorted().collect(toCollection(ArrayList::new));
    }

    private ArrayList<User> findUsersNotPresentInInstrument(String instrumentId) {
        return userRepository.findUsersNotPresentInInstrument(instrumentId)
                .stream().sorted().collect(toCollection(ArrayList::new));
    }

    public ArrayList<User> findUsersByEntity(String entity,
                                             String entityId) {
        switch (entity) {
            case "instrument" -> {
                return findUsersByInstrumentId(entityId);
            }
            case "dataset" -> {
                return findUsersByDatasetId(UUID.fromString(entityId));
            }
            case "admin" -> {
                return findAll();
            }
            default -> throw new IllegalArgumentException(
                    "Given entity (" + entity + ") is not available to filter users by.");

        }
    }

    public ArrayList<User> findUsersByDatasetId(UUID datasetId) {
        return userRepository.findUsersByDatasetId(datasetId)
                .stream().sorted().collect(toCollection(ArrayList::new));
    }

    public ArrayList<User> findUsersByInstrumentId(String instrumentId) {
        return userRepository.findUsersByInstrumentId(instrumentId)
                .stream().sorted().collect(toCollection(ArrayList::new));
    }

    public ArrayList<User> findAll() {
        return userRepository.findAll().stream().sorted().collect(toCollection(ArrayList::new));
    }

    public String getPQData(Authentication authentication, int pq_page, int pq_rpp, String pq_sort, String pq_filter, String filter2, int jt) throws Exception {
        log.debug(String.valueOf(pq_page));
        log.debug(String.valueOf(pq_rpp));
        log.debug(pq_sort);
        log.debug(pq_filter);
        log.debug(filter2);

        return userRepository.getPQGridData(authentication.getName(), pq_page, pq_rpp, pq_sort, pq_filter, filter2, jt);

    }

    public Long getAlertsCount(User user) {
        return alertRepository.countByUserId(user.getEmail());
    }

    public String getAlertsPQData(Authentication authentication, int pq_page, int pq_rpp, String pq_sort, String pq_filter, String filter2, int jt) throws Exception {
        log.debug(String.valueOf(pq_page));
        log.debug(String.valueOf(pq_rpp));
        log.debug(pq_sort);
        log.debug(pq_filter);
        log.debug(filter2);

        return alertRepository.getPQGridData(authentication.getName(), pq_page, pq_rpp, pq_sort, pq_filter, filter2, jt);

    }

    public void updateCardId(User userProfile) {
        userProfile.setCardID(passwordEncoder.encode(userProfile.getCardID()));
        userRepository.save(userProfile);
    }

    public int isLoggedUserAdmin(Authentication authentication) {
        return authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMINISTRATOR")) ?
                GenericRepository.JT_NONE : GenericRepository.JT_USER_ASSOCIATION;
    }
}
