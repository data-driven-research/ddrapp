package cz.tul.cxi.ddr.service;

import cz.tul.cxi.ddr.component.AirflowComponent;
import cz.tul.cxi.ddr.model.postgres.CompositeMeasurementId;
import cz.tul.cxi.ddr.model.elastic.MeasurementMetadata;
import cz.tul.cxi.ddr.model.postgres.*;
import cz.tul.cxi.ddr.repository.MeasurementMetadataRepository;
import cz.tul.cxi.ddr.repository.MeasurementRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static java.util.stream.Collectors.toCollection;

@Service
@Transactional
@Slf4j
public class MeasurementService {
    private final MeasurementRepository measurementRepository;

    private final MeasurementMetadataRepository measurementMetadataRepository;

    private final AirflowComponent airflowComponent;

    public MeasurementService(MeasurementRepository measurementRepository,
                              MeasurementMetadataRepository measurementMetadataRepository,
                              AirflowComponent airflowComponent) {
        this.measurementRepository = measurementRepository;
        this.measurementMetadataRepository = measurementMetadataRepository;
        this.airflowComponent = airflowComponent;
    }

    public boolean measurementExistsByNameAndDataset(Measurement measurement) {
        return measurementRepository.existsByAcronymAndDataset(measurement.getAcronym(), measurement.getDataset());
    }

    public Measurement findMeasurementByFullIdAndUserEmail(CompositeMeasurementId compositeMeasurementId,
                                                           String userEmail) {
        return measurementRepository.findMeasurementByFullIdAndUserEmail(compositeMeasurementId.getMeasurementId(),
                compositeMeasurementId.getRepetition(), userEmail);
    }

    public ArrayList<Measurement> findMeasurementsByEntityAndActivity(String entity,
                                                                      String entityId,
                                                                      Boolean activeOnly) {
        switch (entity) {
            case "dataset" -> {
                return findMeasurementsByDatasetIdAndActivity(UUID.fromString(entityId), activeOnly);
            }
            case "instrument" -> {
                return findMeasurementsByInstrumentIdAndActivity(entityId, activeOnly);
            }
            case "user" -> {
                return findMeasurementsByUserEmailAndActivity(entityId, activeOnly);
            }
            case "admin" -> {
                return findMeasurementsByActivity(activeOnly);
            }
            default -> throw new IllegalArgumentException(
                    "Cannot filter measurements by given entity (" + entity + ")");
        }
    }

    public ArrayList<Measurement> findMeasurementsByInstrumentIdAndActivity(String instrumentId,
                                                                            Boolean activeOnly) {
        if (activeOnly) {
            return measurementRepository.findActiveMeasurementsByInstrumentId(instrumentId)
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        } else {
            return measurementRepository.findMeasurementsByInstrumentId(instrumentId)
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        }
    }

    public ArrayList<Measurement> findMeasurementsByDatasetIdAndActivity(UUID datasetId,
                                                                         Boolean activeOnly) {
        if (activeOnly) {
            return measurementRepository.findActiveMeasurementsByDatasetId(datasetId)
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        } else {
            return measurementRepository.findMeasurementsByDatasetId(datasetId)
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        }
    }

    public ArrayList<Measurement> findMeasurementsByUserEmailAndActivity(String userEmail,
                                                                         Boolean activeOnly) {
        if (activeOnly) {
            return measurementRepository.findActiveMeasurementsByUserEmail(userEmail)
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        } else {
            return measurementRepository.findMeasurementsByUserEmail(userEmail)
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        }
    }

    public ArrayList<Measurement> findMeasurementsByActivity(Boolean activeOnly) {
        if (activeOnly) {
            return measurementRepository.findActiveMeasurements()
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        } else {
            return measurementRepository.findAll()
                    .stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        }
    }

    public Measurement startMeasurement(Measurement measurement,
                                        User user,
                                        boolean repeated) {
        checkAvailabilityOfInstrument(measurement.getInstrument());
        checkRepeatabilityOfMeasurement(measurement);
        if (repeated) {
            measurement.getId().setRepetition(measurement.getId().getRepetition() + 1);
        } else {
            measurement.getId().setMeasurementId(UUID.randomUUID());
            String dataPath = String.format("%s%s/",
                    measurement.getDataset().getDataPath(),
                    measurement.getAcronym());
            measurement.setDataPath(dataPath);
            measurement.setUser(user);
        }
        measurement.setStart(Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague")))));
        measurement.setEnd(null);

        return measurementRepository.save(measurement);
    }

    private void checkRepeatabilityOfMeasurement(Measurement measurement) {
        int repeatabilityEdge = 20;
        if (measurement.getId().getRepetition() > repeatabilityEdge) {
            throw new IllegalStateException(String.format("Measurement %s could be repeated only %s times.",
                    measurement.getAcronym(), repeatabilityEdge));
        }
        if (measurement.getStart() != null
                && LocalDateTime.now().isAfter(measurement.getStart().toLocalDateTime().plusDays(14))) {
            throw new IllegalStateException(String.format("Measurement %s could be repeated only first two weeks after creation.",
                    measurement.getAcronym()));
        }
    }

    private void checkAvailabilityOfInstrument(Instrument instrument) {
        if (instrument.isMeasuring()) {
            throw new IllegalStateException(String.format("Instrument %s is in measuring process.", instrument.getAcronym()));
        }
        if (!instrument.isInstalled()) {
            throw new IllegalStateException(String.format("Instrument %s is not installed yet.", instrument.getAcronym()));
        }
    }

    public void triggerPreparation(Measurement measurement) {
        String dagConf = this.getDagConf(measurement);
        String instrumentOS = (String) measurement.getInstrument().getRegistrationInfo()
                .getOrDefault("pc_os", "Windows");
        String dagID;
        switch (instrumentOS) {
            case "Windows" -> dagID = "measurement_preparation_windows";
            case "Linux" -> dagID = "measurement_preparation_linux";
            default -> throw new IllegalStateException("An unknown type of the instrument OS. " +
                    "Cannot trigger the measurement preparation DAG.");
        }
        airflowComponent.triggerDAG(dagID, dagConf);
    }

    public Measurement endMeasurement(Measurement measurement,
                                      User user) {
        MeasurementMetadata measurementMetadata = createMeasurementMetadata(
                user, measurement, measurement.getInstrument(), measurement.getDataset(),
                measurement.getDataset().getResource());
        if (measurement.getStart().after(measurement.getEnd())
                && measurement.getEnd().after(Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague")))))) {
            throw new IllegalStateException("Invalid end time of measurement.");
        }
        measurementMetadataRepository.save(measurementMetadata);
        measurementRepository.save(measurement);
        return measurementRepository.findById(measurement.getId()).orElseThrow();

    }

    public Measurement updateByEndingMeasurement(Measurement endingMeasurement) {
        Measurement measurement = measurementRepository.findById(endingMeasurement.getId()).orElseThrow();
        measurement.setEnd(endingMeasurement.getEnd());
        measurement.setCustomMetadata(endingMeasurement.getCustomMetadata());

        return measurement;
    }

    private MeasurementMetadata createMeasurementMetadata(User user,
                                                          Measurement measurement,
                                                          Instrument instrument,
                                                          Dataset dataset,
                                                          Resource resource) {
        MeasurementMetadata measurementMetadata = new MeasurementMetadata();
        measurementMetadata.setId(UUID.randomUUID());
        measurementMetadata.setUserEmail(user.getEmail());
        measurementMetadata.setUserDepartment(user.getUDepartmentAcronym());
        measurementMetadata.setMeasurementId(measurement.getId().getMeasurementId());
        measurementMetadata.setMeasurementRepetition(measurement.getId().getRepetition());
        measurementMetadata.setMeasurementAcronym(measurement.getAcronym());
        measurementMetadata.setMeasurementStart(measurement.getStart());
        measurementMetadata.setMeasurementEnd(measurement.getEnd());
        measurementMetadata.setMeasurementDataPath(measurement.getDataPath());
        measurementMetadata.setMeasurementCustomMetadata(measurement.getCustomMetadata());
        measurementMetadata.setInstrumentId(instrument.getId());
        measurementMetadata.setInstrumentNameCZ(instrument.getNameCZ());
        measurementMetadata.setInstrumentNameEN(instrument.getNameEN());
        measurementMetadata.setInstrumentAcronym(instrument.getAcronym());
        measurementMetadata.setInstrumentDepartment(instrument.getIdepartmentAcronym());
        measurementMetadata.setInstrumentCustomMetadata(extractInstrumentCustomMetadata(instrument));
        measurementMetadata.setDatasetId(dataset.getId());
        measurementMetadata.setDatasetNameCZ(dataset.getNameCZ());
        measurementMetadata.setDatasetNameEN(dataset.getNameEN());
        measurementMetadata.setDatasetStart(dataset.getStart());
        measurementMetadata.setDatasetEnd(dataset.getEnd());
        measurementMetadata.setDatasetDataPath(dataset.getDataPath());
        measurementMetadata.setResourceNameCZ(resource.getNameCz());
        measurementMetadata.setResourceNameEN(resource.getNameEn());
        measurementMetadata.setResourceInternalNumber(resource.getInternalNumber());
        measurementMetadata.setResourceActivity(resource.getActivity());
        measurementMetadata.setResourceAcronym(resource.getShortcut());
        measurementMetadata.setResourceDepartment(resource.getDepartment());
        measurementMetadata.setResourceProgram(resource.getProgram());
        measurementMetadata.setResourceType(resource.getType());
        measurementMetadata.setResourceDataPath(resource.getDataPath());

        return measurementMetadata;
    }

    private Map<String, Object> extractInstrumentCustomMetadata(Instrument instrument) {
        return new HashMap<>() {{
            List<String> keys = List.of("pc_os", "root_directory", "open_format", "measurement_programs", "data_processing_programs");
            for (String key : keys) {
                putIfAbsent(key, instrument.getRegistrationInfo().
                        getOrDefault(key, ""));
            }
        }};
    }

    public void triggerBackup(Measurement measurement) {
        String dagConf = this.getDagConf(measurement);
        String instrumentOS = (String) measurement.getInstrument().getRegistrationInfo()
                .getOrDefault("pc_os", "Windows");
        String dagID;
        switch (instrumentOS) {
            case "Windows" -> dagID = "measurement_backup_windows";
            case "Linux" -> dagID = "measurement_backup_linux";
            default -> throw new IllegalStateException("An unknown type of the instrument OS. " +
                    "Cannot trigger the measurement backup DAG.");
        }
        airflowComponent.triggerDAG(dagID, dagConf);
    }

    private String getDagConf(Measurement measurement) {
        return String.format("""
                        {
                          "conf": {
                          "measurement_id": "%s",
                          "instrument_id": "%s",
                          "data_path": "%s",
                          "bucket_name": "%s",
                          "key_prefix": "%s",
                          "root_dir": "%s",
                          "bulk_dir": "%s"
                          }
                        }
                        """,
                measurement.getId().getMeasurementId(),
                measurement.getInstrument().getId(),
                measurement.getDataPath(),
                measurement.getDataPath().substring(0, measurement.getDataPath().indexOf("/")),
                measurement.getDataPath().substring(measurement.getDataPath().indexOf("/") + 1),
                measurement.getInstrument().getRegistrationInfo()
                        .getOrDefault("root_directory", ""),
                measurement.getInstrument().getRegistrationInfo()
                        .getOrDefault("bulk_directory", ""));
    }

    public Measurement createDummyMeasurement(String entity,
                                              String entityId,
                                              String byAcronym) {
        Measurement measurement = new Measurement();
        switch (entity) {
            case "instrument" -> {
                measurement.setInstrument(new Instrument());
                measurement.getInstrument().setId(entityId);
                measurement.getInstrument().setAcronym(byAcronym);
            }
            case "dataset" -> {
                measurement.setDataset(new Dataset());
                measurement.getDataset().setId(UUID.fromString(entityId));
                measurement.getDataset().setAcronym(byAcronym);
            }
            case "user" -> {
                measurement.setUser(new User());
                measurement.getUser().setEmail(entityId);
            }
            default -> throw new IllegalArgumentException(
                    "Cannot preselect given entity (" + entity + ") for measurement.");
        }
        return measurement;
    }

    public String getPQData(Authentication authentication, int pq_page, int pq_rpp, String pq_sort, String pq_filter, String filter2, int jt) throws Exception {
        log.debug(String.valueOf(pq_page));
        log.debug(String.valueOf(pq_rpp));
        log.debug(pq_sort);
        log.debug(pq_filter);
        log.debug(filter2);

        return measurementRepository.getPQGridData(authentication.getName(), pq_page, pq_rpp, pq_sort, pq_filter, filter2, jt);
    }
}
