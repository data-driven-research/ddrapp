package cz.tul.cxi.ddr.service;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.tul.cxi.ddr.component.EmailComponent;
import cz.tul.cxi.ddr.component.S3CoreComponent;
import cz.tul.cxi.ddr.model.postgres.Instrument;
import cz.tul.cxi.ddr.model.postgres.User;
import cz.tul.cxi.ddr.repository.InstrumentRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class InstrumentService {
    private final InstrumentRepository instrumentRepository;
    private final UserService userService;
    private final EmailComponent emailComponent;
    private final S3CoreComponent s3CoreComponent;
    private final ObjectMapper mapper;

    @Value("${s3.cdn_bucket_name}")
    private String cdnBucketName;

    @Value("${s3.instrument_data_bucket_name}")
    private String instrumentDataBucketName;

    public InstrumentService(InstrumentRepository instrumentRepository,
                             UserService userService,
                             EmailComponent emailComponent,
                             S3CoreComponent s3CoreComponent,
                             ObjectMapper mapper) {
        this.instrumentRepository = instrumentRepository;
        this.userService = userService;
        this.emailComponent = emailComponent;
        this.s3CoreComponent = s3CoreComponent;
        this.mapper = mapper;
    }

    public Boolean instrumentExists(Instrument instrument) {
        return instrumentRepository.existsById(instrument.getId());
    }

    public Boolean instrumentExistsByNames(Instrument instrument) {
        return instrumentRepository.existsByNameCZ(instrument.getNameCZ()) || instrumentRepository.existsByNameEN(instrument.getNameEN()) || instrumentRepository.existsByAcronym(instrument.getAcronym());
    }

    public Instrument saveInstrument(User user, Instrument instrument, String fileName) {
        String imageUrl = s3CoreComponent.constructFullURL(cdnBucketName, fileName);
        instrument.setImageURL(imageUrl);
        instrument.setMeasuring(false);
        instrument.getUsers().add(userService.findById(user.getEmail()).orElseThrow());
        return instrumentRepository.save(instrument);
    }

    public void setMeasuring(String instrumentId, boolean measuring) {
        if (instrumentRepository.setMeasuring(instrumentId, measuring) < 1) {
            throw new IllegalStateException("Instrument measuring attribute was not changed successfully.");
        }
    }

    public void setInstalled(String instrumentId, boolean installed) {
        if (instrumentRepository.setInstalled(instrumentId, installed) < 1) {
            throw new IllegalStateException("Instrument installed attribute was not changed successfully.");
        }
    }

    public boolean isConnectible(Instrument instrument) {
        return Objects.equals(instrument.getRegistrationInfo().getOrDefault("real_time", "yes"), "no");
    }

    public void sendInstrumentInstallationRequest(User user, Instrument instrument) throws JsonProcessingException {
        String instrumentAsJSON = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(instrument);

        String subject = "Installation request of a new instrument";
        String text = String.format("User %s from %s requests an installation of the instrument %s %s\n\n %s", user.getEmail(), user.getUDepartmentAcronym(), instrument.getId(), instrument.getNameCZ(), instrumentAsJSON);
        emailComponent.sendSimpleMessage(subject, text, user.getEmail());
    }

    public Resource downloadLog(String keyName) {
        String datasetName = "logs/";
        return s3CoreComponent.getS3File(instrumentDataBucketName, datasetName + keyName);
    }

    public String saveImage(Instrument instrument, MultipartFile image) {
        String keyName = StringUtils.cleanPath("instrument-" + instrument.getId() + "." + Objects.requireNonNull(image.getContentType()).split("/")[1]);
        keyName = keyName.replace(" ", "_");
        String datasetName = "images/";
        s3CoreComponent.putS3File(cdnBucketName, datasetName + keyName, image);

        return keyName;
    }

    public Optional<Instrument> findById(String instrumentId) {
        return instrumentRepository.findById(instrumentId);
    }

    public Instrument findInstrumentByIdAndUserEmail(String instrumentId, String userEmail) {
        Optional<Instrument> optInstrument = instrumentRepository.findInstrumentByIdAndUserEmail(instrumentId, userEmail);
        if (optInstrument.isEmpty()) {
            throw new MissingResourceException("Instrument " + instrumentId + " not found", "Instrument", "");
        }
        return optInstrument.get();
    }

    public ArrayList<Instrument> findInstrumentsByUserAndAvailability(String userEmail, boolean availableOnly) {
        if (availableOnly) {
            return instrumentRepository.findAvailableInstrumentsByUserEmail(userEmail).stream().sorted().collect(toCollection(ArrayList::new));
        } else {
            return instrumentRepository.findInstrumentsByUserEmail(userEmail).stream().sorted().collect(toCollection(ArrayList::new));
        }
    }

    public void addUserToInstrument(String userEmail, String instrumentId) {
        Instrument instrument = instrumentRepository.findById(instrumentId).orElseThrow();
        instrument.getUsers().add(userService.findById(userEmail).orElseThrow());
        instrumentRepository.save(instrument);
    }

    public boolean isInstrumentAvailableOfUser(String instrumentId, String userEmail) {
        Optional<Instrument> optInstrument = instrumentRepository.findInstrumentByIdAndUserEmail(instrumentId, userEmail);

        return optInstrument.isPresent() && optInstrument.get().isInstalled() && !optInstrument.get().isMeasuring();
    }

    public void removeUserFromInstrument(String userEmail, String instrumentId) {
        Instrument instrument = instrumentRepository.findById(instrumentId).orElseThrow();
        instrument.getUsers().remove(userService.findById(userEmail).orElseThrow());
        instrumentRepository.save(instrument);
    }

    public String getPQData(Authentication authentication, int pq_page, int pq_rpp, String pq_sort, String pq_filter, String filter2, int jt) throws Exception {
        log.debug(String.valueOf(pq_page));
        log.debug(String.valueOf(pq_rpp));
        log.debug(pq_sort);
        log.debug(pq_filter);
        log.debug(filter2);

        // return instrumentRepository.getPQGridData(user, pq_page, pq_rpp, pq_sortjson, pq_filterjson, filter2json, (user.isAdmin()) ? GenericRepository.JT_NONE : GenericRepository.JT_USER_ASSOCIATION);
        return instrumentRepository.getPQGridData(authentication.getName(), pq_page, pq_rpp, pq_sort, pq_filter, filter2, jt);

    }

}
