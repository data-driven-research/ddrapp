package cz.tul.cxi.ddr.service;

import static java.util.stream.Collectors.toCollection;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import cz.tul.cxi.ddr.model.s3.S3Object;
import org.springframework.core.io.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.tul.cxi.ddr.model.postgres.Dataset;
import cz.tul.cxi.ddr.model.elastic.MeasurementMetadata;
import cz.tul.cxi.ddr.model.postgres.User;
import cz.tul.cxi.ddr.repository.DatasetRepository;
import cz.tul.cxi.ddr.repository.MeasurementMetadataRepository;
import cz.tul.cxi.ddr.repository.MeasurementRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class DatasetService {
    private final DatasetRepository datasetRepository;
    private final UserService userService;
    private final MeasurementRepository measurementRepository;
    private final MeasurementMetadataRepository measurementMetadataRepository;

    public DatasetService(DatasetRepository datasetRepository,
                          UserService userService,
                          MeasurementRepository measurementRepository,
                          MeasurementMetadataRepository measurementMetadataRepository) {
        this.datasetRepository = datasetRepository;
        this.userService = userService;
        this.measurementRepository = measurementRepository;
        this.measurementMetadataRepository = measurementMetadataRepository;
    }

    public boolean datasetExistsByNamesAndResource(Dataset dataset) {
        return datasetRepository.existsByNameCZAndResource(dataset.getNameCZ(), dataset.getResource()) || datasetRepository.existsByNameENAndResource(dataset.getNameEN(), dataset.getResource())
                || datasetRepository.existsByAcronymAndResource(dataset.getAcronym(), dataset.getResource());
    }

    public Optional<Dataset> findById(UUID datasetId) {
        return datasetRepository.findById(datasetId);
    }

    public Dataset saveDataset(User user, Dataset dataset) {
        if (dataset.getId() == null) {
            dataset.setId(UUID.randomUUID());
        }
        String dataPath = String.format("%s%s/", dataset.getResource().getDataPath(), dataset.getAcronym());
        dataset.setDataPath(dataPath);
        dataset.setStart(Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague")))));
        dataset.getUsers().add(userService.findById(user.getEmail()).orElseThrow());

        return datasetRepository.save(dataset);

    }

    public void closeDataset(UUID datasetId) {
        if (measurementRepository.countActiveMeasurementsByDatasetId(datasetId) > 0) {
            throw new IllegalStateException("Cannot close dataset with running measurement(s).");
        }
        Timestamp now = Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague"))));
        if (datasetRepository.setEnd(datasetId, now) < 1) {
            throw new IllegalStateException("Dataset was not ended successfully.");
        }

        Set<MeasurementMetadata> measMetadata = measurementMetadataRepository.findAllByDatasetId(datasetId);
        measMetadata.forEach(it -> it.setDatasetEnd(now));
        measurementMetadataRepository.saveAll(measMetadata);
    }

    public ArrayList<Dataset> findDatasetsByEntityAndActivity(String entity, String entityId, Boolean activeOnly) {
        switch (entity) {
            case "user" -> {
                return findDatasetsByUserAndActivity(entityId, activeOnly);
            }
            case "admin" -> {
                return findDatasetsByActivity(activeOnly);
            }
            default ->
                    throw new IllegalArgumentException("Given entity (" + entity + ") is not available to filter users by.");

        }
    }

    public ArrayList<Dataset> findDatasetsByUserAndActivity(String userEmail, Boolean activeOnly) {
        if (activeOnly) {
            return datasetRepository.findActiveDatasetsByUserEmail(userEmail).stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        } else {
            return datasetRepository.findDatasetsByUserEmail(userEmail).stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        }
    }

    private ArrayList<Dataset> findDatasetsByActivity(Boolean activeOnly) {
        if (activeOnly) {
            return datasetRepository.findActiveDatasets().stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        } else {
            return datasetRepository.findAll().stream().sorted(Comparator.reverseOrder()).collect(toCollection(ArrayList::new));
        }
    }

    public void addUserToDataset(String userEmail, UUID datasetId) {
        Dataset dataset = datasetRepository.findById(datasetId).orElseThrow();
        dataset.getUsers().add(userService.findById(userEmail).orElseThrow());
        datasetRepository.save(dataset);
    }

    public boolean datasetIsAvailable(UUID datasetId, String userEmail) {
        Optional<Dataset> optDataset = datasetRepository.findDatasetByIdAndUserEmail(datasetId, userEmail);
        return optDataset.isPresent() && optDataset.get().getEnd() == null;
    }

    public String getPQData(Authentication authentication, int pq_page, int pq_rpp, String pq_sort, String pq_filter, String filter2, int jt) throws Exception {
        log.debug(String.valueOf(pq_page));
        log.debug(String.valueOf(pq_rpp));
        log.debug(pq_sort);
        log.debug(pq_filter);
        log.debug(filter2);

        return datasetRepository.getPQGridData(authentication.getName(), pq_page, pq_rpp, pq_sort, pq_filter, filter2, jt);
    }

    public void addUserToS3Dataset(String userEmail, String dataPath) {
        Dataset dataset = datasetRepository.findByDataPath(dataPath).orElseThrow();
        dataset.getUsers().add(userService.findById(userEmail).orElseThrow());
        datasetRepository.save(dataset);
    }
}
