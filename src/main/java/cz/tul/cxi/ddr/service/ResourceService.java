package cz.tul.cxi.ddr.service;

import cz.tul.cxi.ddr.component.GaPComponent;
import cz.tul.cxi.ddr.model.postgres.Resource;
import cz.tul.cxi.ddr.repository.ResourceRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Optional;

import static java.util.stream.Collectors.toCollection;

@Service
public class ResourceService {
    private final ResourceRepository repository;

    private final GaPComponent gaPComponent;

    public ResourceService(ResourceRepository repository,
                           GaPComponent gaPComponent) {
        this.repository = repository;
        this.gaPComponent = gaPComponent;
    }

    public Resource save(Resource resource) {
        if (resource.getInternalNumber().isEmpty() || resource.getActivity().isEmpty()) {
            throw new IllegalStateException("Resource missing internal number or activity.");
        }
        if (resource.getShortcut().isEmpty()) {
            resource.setShortcut("na");
        }
        String dataPath = String.format("%s-%s-%s/",
                resource.getInternalNumber().replace("/", ""),
                resource.getActivity(),
                resource.getShortcut()).toLowerCase(Locale.forLanguageTag("cs"));
        resource.setDataPath(dataPath);
        return repository.save(resource);
    }


    public Boolean resourceExists(Resource resource) {
        return repository.existsByInternalNumberAndActivity(resource.getInternalNumber(), resource.getActivity());
    }

    public ArrayList<Resource> findAll() {
        return repository.findAll()
                .stream().sorted().collect(toCollection(ArrayList::new));
    }

    public Resource[] loadGaPResources(String internalNumber,
                                       String activity) {
        Resource[] resources = gaPComponent.loadResources(internalNumber, activity);
        if (resources.length < 1) {
            throw new MissingResourceException("Resource " + internalNumber + " was not found.",
                    "Resource", "");
        }
        return resources;
    }

    public Optional<Resource> findById(Long resourceId) {
        return repository.findById(resourceId);
    }
}
