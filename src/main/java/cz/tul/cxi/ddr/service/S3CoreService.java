package cz.tul.cxi.ddr.service;

import cz.tul.cxi.ddr.component.S3CoreComponent;
import cz.tul.cxi.ddr.model.postgres.Dataset;
import cz.tul.cxi.ddr.model.postgres.User;
import cz.tul.cxi.ddr.model.s3.S3Bucket;
import cz.tul.cxi.ddr.model.s3.S3Object;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class S3CoreService {

    private final S3CoreComponent s3CoreComponent;

    public S3CoreService(S3CoreComponent s3CoreComponent) {
        this.s3CoreComponent = s3CoreComponent;
    }

    public String listBuckets() {
        log.debug("Listing buckets");
        String buckets = s3CoreComponent.listBuckets();
        log.debug("Buckets: " + buckets);

        return buckets;
    }

    public String listDatasets(S3Bucket s3Bucket, User user) {
        log.debug("Listing datasets");
        String datasets = s3CoreComponent.listDatasets(s3Bucket, user.getEinfraId());
        log.debug("Datasets: " + datasets);

        return datasets;
    }

    public String listFiles(S3Object s3Dataset, User user) {
        log.debug("Listing files");
        String files = s3CoreComponent.listObjects(s3Dataset, user.getEinfraId(), true);
        log.debug("Files: " + files);

        return files;
    }

    public String downloadS3Dataset(S3Object s3Dataset, User user) {
        log.debug("Downloading dataset");
        String urls = s3CoreComponent.getPreSignedULRS(s3Dataset, user.getEinfraId());
        log.debug("File: " + urls);

        return urls;
    }

    public String downloadFile(S3Object s3Object, User user) {
        log.debug("Downloading file");
        String urls = s3CoreComponent.downloadFile(s3Object, user.getEinfraId());
        log.debug("File: " + urls);

        return urls;
    }

    public void addUserToDataset(User user, Dataset dataset) {
        log.debug("Sharing dataset");
        String[] splitPath = dataset.getDataPath().split("/");
        String bucketName = splitPath[0];
        String prefix = "";
        String name = splitPath[1];
        s3CoreComponent.shareDataset(new S3Object(bucketName, name, prefix), user.getEinfraId());

    }

    public void createBucket(String bucketName) {
        log.debug("Creating bucket");
        s3CoreComponent.createBucket(new S3Bucket(bucketName));
    }

    public void createDataset(Dataset dataset, User user) {
        log.debug("Creating dataset");
        String[] splitPath = dataset.getDataPath().split("/");
        String bucketName = splitPath[0];
        String prefix = "";
        String name = splitPath[1];
        s3CoreComponent.createDataset(new S3Object(bucketName, name, prefix), user.getEinfraId());
    }

    public String getPublicURLs(S3Object s3Dataset, User user) {
        log.debug("Sharing public URLs");
        String urls = s3CoreComponent.getPreSignedULRS(s3Dataset, user.getEinfraId());
        log.debug("URLs: " + urls);

        return urls;
    }
}
