package cz.tul.cxi.ddr.model.s3;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class S3Object {
    @NotBlank(message = "bucketName must not be blank.")
    private String bucketName;

    @NotBlank(message = "(file) name must not be blank.")
    private String name; // current patch of path

    @NotNull(message = "(file) prefix must not be null.")
    private String prefix; // almost full path relative to bucketName
}
