package cz.tul.cxi.ddr.model.auxiliary;

import java.util.List;

public record ConfigOption(String key,
                           List<Choice> choices,
                           InputType inputType) {
}
