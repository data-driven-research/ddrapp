package cz.tul.cxi.ddr.model.auxiliary;

public record Choice(String en,
                     String cs) {
}
