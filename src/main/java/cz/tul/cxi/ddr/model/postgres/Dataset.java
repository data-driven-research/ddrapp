package cz.tul.cxi.ddr.model.postgres;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "datasets")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Dataset implements Comparable<Dataset> {
    @Id
    private UUID id;

    @Column(name = "name_cz",
            nullable = false)
    private String nameCZ;

    @Column(name = "name_en",
            nullable = false)
    private String nameEN;

    @Column(name = "acronym",
            nullable = false)
    private String acronym;

    @JsonManagedReference
    @JsonUnwrapped(prefix = "resource_")
    @ManyToOne
    @JoinColumn(name = "resource_id",
            referencedColumnName = "id",
            nullable = false)
    private Resource resource;

    @Column(name = "start_ts",
            nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Timestamp start;

    @Column(name = "end_ts")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Timestamp end;

    @Column(name = "data_path",
            unique = true)
    private String dataPath;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JsonIgnore
    @JoinTable(name = "user_dataset",
            joinColumns = @JoinColumn(name = "dataset_id"),
            inverseJoinColumns = @JoinColumn(name = "user_email"))
    private Set<User> users = new LinkedHashSet<>();

    @Override
    public int compareTo(Dataset o) {
        return this.start.compareTo(o.start);
    }
}
