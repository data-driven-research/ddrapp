package cz.tul.cxi.ddr.model.postgres;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "resources")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Resource implements Serializable, Comparable<Resource> {
    /**
     *
     */
    @Serial
    private static final long serialVersionUID = -5192600379771926833L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name_cz", nullable = false)
    private String nameCz = "";

    @Column(name = "name_en", nullable = false)
    private String nameEn = "";

    @Column(name = "internal_number", nullable = false)
    private String internalNumber = "";

    @Column(name = "activity")
    private String activity = "";

    @Column(name = "department")
    private String department;

    @Column(name = "program")
    private String program;

    @Column(name = "acronym", nullable = false)
    private String shortcut = "";

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "data_path", unique = true)
    private String dataPath;

    @JsonBackReference
    @OneToMany(mappedBy = "resource",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Dataset> datasets = new ArrayList<>();

    @Override
    public int compareTo(Resource o) {
        return this.getNameEn().compareTo(o.getNameEn());
    }
}
