package cz.tul.cxi.ddr.model.s3;

import java.util.List;

public record BucketPolicy(String Id,
                           String Version,
                           List<cz.tul.cxi.ddr.model.s3.Statement> Statement) {

    public BucketPolicy(List<Statement> Statement) {
        this("PolicyId", "2012-10-17", Statement);
    }
}
