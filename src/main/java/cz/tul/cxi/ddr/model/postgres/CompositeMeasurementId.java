package cz.tul.cxi.ddr.model.postgres;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
public class CompositeMeasurementId implements Serializable {
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id")
    private UUID measurementId;

    @Column(name = "repetition")
    private Integer repetition;

    public CompositeMeasurementId(UUID measurementId,
                                  Integer repetition) {
        this.measurementId = measurementId;
        this.repetition = repetition;
    }
}
