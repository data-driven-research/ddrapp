package cz.tul.cxi.ddr.model.postgres;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.tul.cxi.ddr.model.auxiliary.Choice;
import cz.tul.cxi.ddr.model.auxiliary.ConfigOption;
import cz.tul.cxi.ddr.model.auxiliary.DepartmentAcronym;
import cz.tul.cxi.ddr.model.auxiliary.InputType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.*;

@Entity
@Table(name = "instruments")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Instrument implements Comparable<Instrument> {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name_cz",
            nullable = false,
            unique = true)
    private String nameCZ;

    @Column(name = "name_en",
            nullable = false,
            unique = true)
    private String nameEN;

    @Column(name = "acronym",
            nullable = false,
            unique = true)
    private String acronym;

    @Column(name = "description_cz")
    private String descriptionCZ = "";

    @Column(name = "description_en")
    private String descriptionEN = "";

    @Column(name = "image_url",
            unique = true)
    private String imageURL;

    @Column(name = "measuring",
            nullable = false)
    private boolean measuring = false;

    @Column(name = "installed",
            nullable = false)
    private boolean installed = false;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinTable(name = "user_instrument",
            joinColumns = @JoinColumn(name = "instrument_id"),
            inverseJoinColumns = @JoinColumn(name = "user_email"))
    private Set<User> users = new LinkedHashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "department",
            nullable = false)
    private DepartmentAcronym idepartmentAcronym = DepartmentAcronym.GUEST;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "registration_info",
            columnDefinition = "jsonb",
            nullable = false)
    @ToString.Exclude
    private HashMap<String, Object> registrationInfo = new HashMap<>();

    @Transient
    private @Getter
    static final List<ConfigOption> REGISTRATION_INFO_OPTIONS = List.of(
            new ConfigOption("pc_os",
                    List.of(new Choice("Windows", "Windows"),
                            new Choice("Linux", "Linux")),
                    InputType.SELECTION),
            new ConfigOption("remote_access",
                    List.of(new Choice("control", "ovládání"),
                            new Choice("view", "zobrazení"),
                            new Choice("none", "žádná")),
                    InputType.SELECTION),
            new ConfigOption("pc_os_update_off",
                    List.of(new Choice("yes", "ano"),
                            new Choice("no", "ne")),
                    InputType.RADIO),
            new ConfigOption("antivirus_off",
                    List.of(new Choice("yes", "ano"),
                            new Choice("no", "ne")),
                    InputType.RADIO),
            new ConfigOption("antivirus_exceptions",
                    List.of(),
                    InputType.TEXT_Q),
            new ConfigOption("software_update_URL",
                    List.of(),
                    InputType.TEXT_Q),
            new ConfigOption("instrument_update_URL",
                    List.of(),
                    InputType.TEXT_Q),
            new ConfigOption("root_directory",
                    List.of(),
                    InputType.TEXT_Q),
            new ConfigOption("bulk_directory",
                    List.of(),
                    InputType.TEXT_Q),
            new ConfigOption("open_format",
                    List.of(new Choice("yes", "ano"),
                            new Choice("yes, after export", "ano, po exportu"),
                            new Choice("no", "ne")),
                    InputType.SELECTION),
            new ConfigOption("volume",
                    List.of(new Choice("100 MB", "100 MB"),
                            new Choice("1 GB", "1 GB"),
                            new Choice("10 GB", "10 GB"),
                            new Choice("100 GB", "100 GB"),
                            new Choice("1 TB", "1 TB"),
                            new Choice("10 TB", "10 TB")),
                    InputType.SELECTION),
            new ConfigOption("real_time",
                    List.of(new Choice("yes", "ano"),
                            new Choice("no", "ne")),
                    InputType.RADIO),
            new ConfigOption("data_platform",
                    List.of(new Choice("on department equipment", "na PC oddělení"),
                            new Choice("on the university infrastructure", "na univerzitní infrastruktuře"),
                            new Choice("on the CESNET infrastructure", "na CESNET infrastruktuře"),
                            new Choice("with a commercial cloud provider", "u komerčního poskytovatele cloudu")),
                    InputType.CHECKBOX),
            new ConfigOption("computing_platform",
                    List.of(new Choice("on department equipment", "na PC oddělení"),
                            new Choice("on the university infrastructure", "na univerzitní infrastruktuře"),
                            new Choice("on the CESNET infrastructure", "na CESNET infrastruktuře"),
                            new Choice("with a commercial cloud provider", "u komerčního poskytovatele cloudu")),
                    InputType.CHECKBOX),
            new ConfigOption("measurement_programs",
                    List.of(),
                    InputType.TEXT_Q),
            new ConfigOption("data_processing_programs",
                    List.of(),
                    InputType.TEXT_Q));

    @PrePersist
    @PreUpdate
    protected void onCreate() {
        if (registrationInfo == null) {
            registrationInfo = new HashMap<>();
        }
        REGISTRATION_INFO_OPTIONS.forEach(e -> registrationInfo.putIfAbsent(e.key(), new ArrayList<String>()));
    }

    public boolean isAvailable() {
        return !measuring && installed;
    }

    @Override
    public int compareTo(Instrument o) {
        return this.acronym.toLowerCase(Locale.forLanguageTag("cs")).compareTo(o.acronym.toLowerCase(Locale.forLanguageTag("cs")));
    }
}
