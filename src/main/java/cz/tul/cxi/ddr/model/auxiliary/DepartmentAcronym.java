package cz.tul.cxi.ddr.model.auxiliary;

public enum DepartmentAcronym {
    GUEST,
    LCS,
    LMI,
    LSI,
    OABI,
    OECH,
    OFM,
    OGP,
    OKS,
    OLZ,
    OMI,
    OMS,
    OMR,
    ONCH,
    OPM,
    OPT,
    OPV,
    OSW,
    OTP,
    OTZP,
    OVZ,
    RCX
}
