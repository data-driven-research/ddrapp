package cz.tul.cxi.ddr.model.s3;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class S3Bucket {
    @NotBlank(message = "(bucket) name must not be blank.")
    private String name;
}
