package cz.tul.cxi.ddr.model.elastic;

import cz.tul.cxi.ddr.model.auxiliary.DepartmentAcronym;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Document(indexName = "measurement_metadata")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class MeasurementMetadata {
    @Id
    private UUID id;

    @Field(type = FieldType.Text,
            name = "user_email")
    private String userEmail;

    @Field(type = FieldType.Keyword,
            name = "user_department")
    private DepartmentAcronym userDepartment;

    @Field(type = FieldType.Text,
            name = "measurement_id")
    private UUID measurementId;

    @Field(type = FieldType.Integer,
            name = "measurement_repetition")
    private Integer measurementRepetition;

    @Field(type = FieldType.Text,
            name = "measurement_acronym")
    private String measurementAcronym;

    @Field(type = FieldType.Date,
            format = DateFormat.date_hour_minute_second,
            name = "measurement_start")
    private Date measurementStart;

    @Field(type = FieldType.Date,
            format = DateFormat.date_hour_minute_second,
            name = "measurement_end")
    private Date measurementEnd;

    @Field(type = FieldType.Text,
            name = "measurement_data_path")
    private String measurementDataPath;

    @Field(type = FieldType.Object,
            name = "measurement_custom_metadata")
    private Map<String, Object> measurementCustomMetadata;

    @Field(type = FieldType.Text,
            name = "dataset_id")
    private UUID datasetId;

    @Field(type = FieldType.Text,
            name = "dataset_name_cz")
    private String datasetNameCZ;

    @Field(type = FieldType.Text,
            name = "dataset_name_en")
    private String datasetNameEN;

    @Field(type = FieldType.Date,
            format = DateFormat.date_hour_minute_second,
            name = "dataset_start")
    private Date datasetStart;

    @Field(type = FieldType.Date,
            format = DateFormat.date_hour_minute_second,
            name = "dataset_end")
    private Date datasetEnd;

    @Field(type = FieldType.Text,
            name = "dataset_data_path")
    private String datasetDataPath;

    @Field(type = FieldType.Text,
            name = "resource_name_cz")
    private String resourceNameCZ;

    @Field(type = FieldType.Text,
            name = "resource_name_en")
    private String resourceNameEN;

    @Field(type = FieldType.Text,
            name = "resource_internal_number")
    private String resourceInternalNumber;

    @Field(type = FieldType.Text,
            name = "resource_activity")
    private String resourceActivity;

    @Field(type = FieldType.Text,
            name = "resource_acronym")
    private String resourceAcronym;

    @Field(type = FieldType.Text,
            name = "resource_department")
    private String resourceDepartment;

    @Field(type = FieldType.Text,
            name = "resource_program")
    private String resourceProgram;

    @Field(type = FieldType.Text,
            name = "resource_type")
    private String resourceType;

    @Field(type = FieldType.Text,
            name = "resource_data_path")
    private String resourceDataPath;

    @Field(type = FieldType.Text,
            name = "instrument_id")
    private String instrumentId;

    @Field(type = FieldType.Text,
            name = "instrument_name_cz")
    private String instrumentNameCZ;

    @Field(type = FieldType.Text,
            name = "instrument_name_en")
    private String instrumentNameEN;

    @Field(type = FieldType.Text,
            name = "instrument_acronym")
    private String instrumentAcronym;

    @Field(type = FieldType.Text,
            name = "instrument_department")
    private DepartmentAcronym instrumentDepartment;

    @Field(type = FieldType.Object,
            name = "instrument_custom_metadata")
    private Map<String, Object> instrumentCustomMetadata;
}
