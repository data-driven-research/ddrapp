package cz.tul.cxi.ddr.model.postgres;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "measurements")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Measurement implements Comparable<Measurement> {
    @EmbeddedId
    @JsonUnwrapped(prefix = "id_")
    private CompositeMeasurementId id;

    @Column(name = "acronym",
            nullable = false)
    private String acronym;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_email",
            nullable = false)
    private User user;

    @JsonManagedReference
    @JsonUnwrapped(prefix = "dataset_")
    @ManyToOne
    @JoinColumn(name = "dataset_id",
            nullable = false)
    private Dataset dataset;

    @JsonManagedReference
    @JsonUnwrapped(prefix = "instrument_")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "instrument_id",
            nullable = false)
    private Instrument instrument;

    @Column(name = "start_ts",
            nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Timestamp start;

    @Column(name = "end_ts")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Timestamp end;

    @Column(name = "data_path")
    private String dataPath;

    @Column(name = "uploaded")
    private Boolean uploaded = false;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "custom_metadata", columnDefinition = "jsonb")
    private Map<String, Object> customMetadata = new HashMap<>();

    @PrePersist
    @PreUpdate
    protected void onCreate() {
        if (customMetadata == null) {
            customMetadata = new HashMap<>();
        }
        customMetadata.putIfAbsent("tags", new ArrayList<String>());
    }

    @Override
    public int compareTo(Measurement o) {
        return this.start.compareTo(o.start);
    }
}
