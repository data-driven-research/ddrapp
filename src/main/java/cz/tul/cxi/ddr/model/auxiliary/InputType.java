package cz.tul.cxi.ddr.model.auxiliary;

public enum InputType {
    CHECKBOX,
    RADIO,
    TEXT,
    TEXT_Q,
    SELECTION
}
