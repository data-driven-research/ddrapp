package cz.tul.cxi.ddr.utility;

import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;

import java.time.Duration;

public class StringToDurationConverter
        implements Converter<String, Duration> {

    @Override
    public Duration convert(@NonNull String from) {
        return Duration.ofMinutes(Long.parseLong(from));
    }
}
