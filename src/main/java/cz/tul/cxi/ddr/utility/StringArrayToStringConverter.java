package cz.tul.cxi.ddr.utility;

import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;

public class StringArrayToStringConverter
        implements Converter<String[], String> {

    @Override
    public String convert(@NonNull String[] from) {
        return String.join("", from);
    }
}
