package cz.tul.cxi.ddr.rest;

import cz.tul.cxi.ddr.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/user")
public class UserREST {

    private final UserService userService;

    public UserREST(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/pqgrid", produces = {"application/json"})
    public ResponseEntity<Object> pqgrid(
            Authentication authentication,
            @Nullable @RequestParam("pq_curpage") Long pq_page,
            @Nullable @RequestParam("pq_rpp") Long pq_limit,
            @Nullable @RequestParam("pq_sort") String pq_sort,
            @Nullable @RequestParam("pq_filter") String pq_filter,
            @Nullable @RequestParam("filter2") String filter2) {

        try {
            final int pp = (pq_page == null) ? 0 : pq_page.intValue();
            final int pl = (pq_limit == null) ? 0 : pq_limit.intValue();
            int jt = userService.isLoggedUserAdmin(authentication);
            final String result = userService.getPQData(authentication, pp, pl, pq_sort, pq_filter, filter2, jt);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/alert/count", produces = {"application/json"})
    public ResponseEntity<Object> alertsCount(Authentication authentication) {
        try {
            final Long result = userService.getAlertsCount(userService.findById(authentication.getName()).orElseThrow());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(value = "/alert/pqgrid", produces = {"application/json"})
    public ResponseEntity<Object> alertsPqgrid(
            Authentication authentication,
            @Nullable @RequestParam("pq_curpage") Long pq_page,
            @Nullable @RequestParam("pq_rpp") Long pq_limit,
            @Nullable @RequestParam("pq_sort") String pq_sort,
            @Nullable @RequestParam("pq_filter") String pq_filter,
            @Nullable @RequestParam("filter2") String filter2) {

        try {
            final int pp = (pq_page == null) ? 0 : pq_page.intValue();
            final int pl = (pq_limit == null) ? 0 : pq_limit.intValue();
            int jt = userService.isLoggedUserAdmin(authentication);
            final String result = userService.getAlertsPQData(authentication, pp, pl, pq_sort, pq_filter, filter2, jt);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
