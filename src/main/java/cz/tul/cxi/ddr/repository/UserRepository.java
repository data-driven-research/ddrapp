package cz.tul.cxi.ddr.repository;

import cz.tul.cxi.ddr.model.postgres.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.UUID;

@Repository
public interface UserRepository extends GenericRepository<User, String> {
    @Query(value = "SELECT * FROM users u " +
            "INNER JOIN user_instrument ui ON ui.user_email = u.email " +
            "WHERE ui.instrument_id = ?1", nativeQuery = true)
    HashSet<User> findUsersByInstrumentId(String instrumentId);

    @Query(value = "SELECT * FROM users u " +
            "INNER JOIN user_dataset ud ON ud.user_email = u.email " +
            "WHERE ud.dataset_id = ?1", nativeQuery = true)
    HashSet<User> findUsersByDatasetId(UUID datasetId);

    @Query(value = "SELECT * FROM users u " +
            "WHERE u.email NOT IN (" +
            "SELECT ud.user_email " +
            "FROM user_dataset ud " +
            "WHERE ud.dataset_id = ?1" +
            ")", nativeQuery = true)
    HashSet<User> findUsersNotPresentInDataset(UUID datasetId);

    @Query(value = "SELECT * FROM users u " +
            "WHERE u.email NOT IN (" +
            "SELECT ui.user_email " +
            "FROM user_instrument ui " +
            "WHERE ui.instrument_id = ?1" +
            ")", nativeQuery = true)
    HashSet<User> findUsersNotPresentInInstrument(String instrumentId);


}
