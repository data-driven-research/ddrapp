package cz.tul.cxi.ddr.repository;

import cz.tul.cxi.ddr.model.postgres.Resource;
import org.springframework.stereotype.Repository;


@Repository
public interface ResourceRepository extends GenericRepository<Resource, Long> {
    boolean existsByInternalNumberAndActivity(String internalNumber,
                                              String activity);
}
