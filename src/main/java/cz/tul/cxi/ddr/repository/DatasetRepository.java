package cz.tul.cxi.ddr.repository;

import cz.tul.cxi.ddr.model.postgres.Dataset;
import cz.tul.cxi.ddr.model.postgres.Resource;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DatasetRepository extends GenericRepository<Dataset, UUID> {
    boolean existsByNameCZAndResource(String nameCZ, Resource resource);

    boolean existsByNameENAndResource(String nameEn, Resource resource);

    boolean existsByAcronymAndResource(String acronym, Resource resource);

    @Query(value = "SELECT * " +
            "FROM datasets d " +
            "INNER JOIN user_dataset ud ON ud.dataset_id = d.id " +
            "WHERE ud.user_email = ?1 AND d.end_ts IS NULL", nativeQuery = true)
    HashSet<Dataset> findActiveDatasetsByUserEmail(String userEmail);


    @Query(value = "SELECT * FROM datasets d " +
            "INNER JOIN user_dataset ud ON ud.dataset_id = d.id " +
            "WHERE ud.user_email = ?1", nativeQuery = true)
    HashSet<Dataset> findDatasetsByUserEmail(String userEmail);

    @Query(value = "SELECT * " +
            "FROM datasets d " +
            "WHERE d.end_ts IS NULL", nativeQuery = true)
    HashSet<Dataset> findActiveDatasets();

    @Modifying
    @Query(value = "UPDATE datasets " +
            "SET end = ?2 " +
            "WHERE id = ?1", nativeQuery = true)
    int setEnd(UUID datasetId, Timestamp end);

    @Query(value = "SELECT * FROM datasets d " +
            "INNER JOIN user_dataset ud ON ud.dataset_id = d.id " +
            "WHERE ud.user_email = ?2 AND d.id = ?1", nativeQuery = true)
    Optional<Dataset> findDatasetByIdAndUserEmail(UUID datasetId, String userEmail);

    Optional<Dataset> findByDataPath(String dataPath);
}
