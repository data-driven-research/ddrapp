package cz.tul.cxi.ddr.repository;

import cz.tul.cxi.ddr.model.postgres.Dataset;
import cz.tul.cxi.ddr.model.postgres.Measurement;
import cz.tul.cxi.ddr.model.postgres.CompositeMeasurementId;
import org.springframework.data.jpa.repository.Query;

import java.util.HashSet;
import java.util.UUID;

public interface MeasurementRepository extends GenericRepository<Measurement, CompositeMeasurementId> {
    String lastRepetition =
            "GROUP BY m.id, m.repetition " +
                    "HAVING (m.id, m.repetition) IN " +
                    "(SELECT n.id, MAX(n.repetition) FROM measurements n GROUP BY n.id)";

    boolean existsByAcronymAndDataset(String acronym,
                                      Dataset dataset);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.instrument_id = ?1 AND m.end_ts IS NULL " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findActiveMeasurementsByInstrumentId(String instrumentId);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.instrument_id = ?1 " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findMeasurementsByInstrumentId(String instrumentId);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.dataset_id = ?1 AND m.end_ts IS NULL " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findActiveMeasurementsByDatasetId(UUID datasetId);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.dataset_id = ?1 " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findMeasurementsByDatasetId(UUID datasetId);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.end_ts IS NULL " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findActiveMeasurements();

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.id = ?1 AND m.repetition = ?2 AND m.user_email = ?3", nativeQuery = true)
    Measurement findMeasurementByFullIdAndUserEmail(UUID measurementId,
                                                    Integer repetition,
                                                    String userEmail);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.user_email = ?1 AND m.end_ts IS NULL " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findActiveMeasurementsByUserEmail(String userEmail);

    @Query(value = "SELECT * " +
            "FROM measurements m " +
            "WHERE m.user_email = ?1 " +
            lastRepetition, nativeQuery = true)
    HashSet<Measurement> findMeasurementsByUserEmail(String userEmail);

    @Query(value = "SELECT COUNT(m) " +
            "FROM measurements m " +
            "WHERE m.dataset_id = ?1 AND m.end_ts IS NULL", nativeQuery = true)
    int countActiveMeasurementsByDatasetId(UUID datasetId);
}
