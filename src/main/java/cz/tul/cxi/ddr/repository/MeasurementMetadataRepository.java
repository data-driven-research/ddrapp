package cz.tul.cxi.ddr.repository;

import cz.tul.cxi.ddr.model.elastic.MeasurementMetadata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.HashSet;
import java.util.UUID;

public interface MeasurementMetadataRepository extends ElasticsearchRepository<MeasurementMetadata, UUID> {
    HashSet<MeasurementMetadata> findAllByDatasetId(UUID datasetId);
}
