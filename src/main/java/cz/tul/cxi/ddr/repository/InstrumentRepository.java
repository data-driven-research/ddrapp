package cz.tul.cxi.ddr.repository;

import java.util.HashSet;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cz.tul.cxi.ddr.model.postgres.Instrument;

@Repository
public interface InstrumentRepository extends GenericRepository<Instrument, String> {

    boolean existsByNameCZ(String nameCZ);

    boolean existsByNameEN(String nameEN);

    boolean existsByAcronym(String acronym);

    @Query(value = "SELECT * FROM instruments i INNER JOIN user_instrument ui ON ui.instrument_id = i.id WHERE ui.user_email = ?1", nativeQuery = true)
    HashSet<Instrument> findInstrumentsByUserEmail(String userEmail);

    @Query(value = "SELECT * FROM instruments i INNER JOIN user_instrument ui ON ui.instrument_id = i.id WHERE ui.user_email = ?1 AND i.measuring = FALSE AND i.installed = TRUE", nativeQuery = true)
    HashSet<Instrument> findAvailableInstrumentsByUserEmail(String userEmail);

    @Query(value = "SELECT * FROM instruments i INNER JOIN user_instrument ui ON ui.instrument_id = i.id WHERE ui.user_email = ?2 AND ui.instrument_id = ?1", nativeQuery = true)
    Optional<Instrument> findInstrumentByIdAndUserEmail(String instrumentId, String userEmail);

    @Modifying
    @Query(value = "UPDATE instruments SET measuring = ?2 WHERE id = ?1 AND installed = TRUE", nativeQuery = true)
    int setMeasuring(String instrumentId, boolean measuring);

    @Modifying
    @Query(value = "UPDATE instruments SET installed = ?2 WHERE id = ?1", nativeQuery = true)
    int setInstalled(String instrumentId, boolean installed);
}
