package cz.tul.cxi.ddr.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cz.tul.cxi.ddr.model.postgres.Alert;

@Repository
public interface AlertRepository extends GenericRepository<Alert, Long> {
    @Query("SELECT COUNT(a) FROM Alert a JOIN User u ON u.email = a.user.email WHERE a.user.email = :userId")
    Long countByUserId(String userId);
}
