package cz.tul.cxi.ddr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface GenericRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

    int JT_NONE = 0;
    int JT_USER_ASSOCIATION = 1;

    String getPQGridData(String userEmail, int page, int limit, String pq_sort, String pq_filter, String pq_filter2, int joinTable) throws Exception;

}
