function isMobileDevice() {
  var mobileRegEx = new RegExp("Android|webOS|iPhone|iPad|iPod|BlackBerry", "i");
  return mobileRegEx.test(navigator.userAgent);
}

function setSizes(v, h) {
  var dv = $(window).width();
  if (dv > v) dv=v;
  var dh = $(window).height();
  if (dh > h) dh=h;
  return {width: dv, height: dh};
}

function uuid() {
	var tstmp = new Date();
	return tstmp.getTime();
}

function downloadAndZip(data) {
        const zip = new JSZip();
        const frontendDomain = window.location.origin;
        console.log(frontendDomain);
        const promises = data.fileUrls.map(async fileUrl => {
            try {
                const filename = fileUrl.prefix + fileUrl.url.substring(fileUrl.url.lastIndexOf('/') + 1, fileUrl.url.indexOf('?'));
                const response = await fetch(fileUrl.url, {
                                                    method: 'GET',
                                                    mode: 'cors',
                                                    cache: 'no-cache',
                                                    headers: {
                                                       Origin: window.location.origin,
                                                    },
                                                  });
                if (!response.ok) {
                    throw new Error(`Failed to fetch ${fileUrl.url}: ${response.statusText}`);
                }
                const blob = await response.blob();
                if (!blob.size) {
                    throw new Error(`Failed to create blob for ${fileUrl.url}`);
                }
                zip.file(filename, blob, { createFolders: true });
            } catch (error) {
                console.error(error);
                throw error; // Rethrow error to catch it later
            }
        });

        // Use Promise.all to wait for all promises to resolve
        Promise.all(promises).then(() => {
            zip.generateAsync({ type: 'blob' }).then(content => {
                const blobUrl = URL.createObjectURL(content);
                const link = document.createElement('a');
                link.href = blobUrl;
                link.download = 's3dataset.zip';
                link.click();
                URL.revokeObjectURL(blobUrl);
            }).catch(error => {
                console.error('Failed to generate zip file:', error);
            });
        }).catch(error => {
            console.error('Failed to add files to zip:', error);
        });
}

function downloadFile(data) {
        const frontendDomain = window.location.origin;
        console.log(frontendDomain);
        const url = data.url

        const filename = url.substring(url.lastIndexOf('/') + 1, url.indexOf('?'));
        fetch(url, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                 Origin: window.location.origin,
            },
            }).then(response => {
                    if (!response.ok) {
                        throw new Error(`Failed to fetch ${url}: ${response.statusText}`);
                    }
                    return response.blob();
                }).then(blob => {
                    if (!blob.size) {
                        throw new Error(`Failed to create blob for ${url}`);
                    }
                    const blobUrl = URL.createObjectURL(blob);
                    const link = document.createElement('a');
                    link.href = blobUrl;
                    link.download = filename;
                    link.click();
                    URL.revokeObjectURL(blobUrl);
                }).catch(error => {
                    console.error('Failed to download file:', error);
        });
}

$(document).ready(function() {
	
	if ($.datepicker)	{	
		$.datepicker.regional['en'] = {
			closeText : 'Close',
			prevText : 'Previous',
			nextText : 'Next',
			currentText : 'Now',
			monthNames : [
				'january', 'february', 'march', 'april', 'may', 'june', 'july',	'august', 'september', 'october', 'november', 'december'
			],
			monthNamesShort : [
				'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
			],
			dayNames : [
				'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'
			],
			dayNamesShort : [
				'su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'
			],
			dayNamesMin : [
				'su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'
			],
			weekHeader : 'KW',
			dateFormat : "dd.MM.yyyy".toLowerCase().replaceAll('yyyy', 'yy'), 
			firstDay : 0,
			isRTL : false,
			showMonthAfterYear : false,
			yearSuffix : '',
			numberOfMonths : 1,
			changeMonth: true,
			changeYear: true,
	    yearRange: "-20Y:+1Y",
	  	beforeShowDay: weekendOrHoliday,
		};
		$.datepicker.regional['cs'] = {
			closeText : 'Zavřít',
			prevText : 'Předchozí',
			nextText : 'Následující',
			currentText : 'Nyní',
			monthNames : [
				'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec',	'srpen', 'září', 'říjen', 'listopad', 'prosinec'
			],
			monthNamesShort : [
				'led', 'úno', 'bře', 'dub', 'kvě', 'čer', 'čvc', 'srp', 'zář', 'říj', 'lis', 'pro'
			],
			dayNames : [
				'neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'
			],
			dayNamesShort : [
				'ne', 'po', 'út', 'st', 'čt', 'pá', 'so'
			],
			dayNamesMin : [
				'ne', 'po', 'út', 'st', 'čt', 'pá', 'so'
			],
			weekHeader : 'KT',
			dateFormat : "yyyy-MM-dd".toLowerCase().replaceAll('yyyy', 'yy'),
			firstDay : 1,
			isRTL : false,
			showMonthAfterYear : false,
			yearSuffix : '',
			numberOfMonths : 1,
			changeMonth: true,
			changeYear: true,
	    yearRange: "-20Y:+1Y",
	  	beforeShowDay: weekendOrHoliday,
		};
		$.datepicker.setDefaults($.datepicker.regional['cs']);
	}
	
	if ($.timepicker)	{	
		$.timepicker.regional['cs'] = {
			timeOnlyTitle : 'Zvolte čas',
			timeText : 'Čas',
			hourText : 'Hod',
			minuteText : 'Min',
			secondText : 'Sec',
			millisecText : 'ms',
			currentText : 'Nyní',
			closeText : 'OK',
			ampm : false,
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm',
			showButtonPanel : false
		};
		$.timepicker.setDefaults($.timepicker.regional['cs']);
	}
	
	if ($.paramquery)	{	
		var pageModel = {
	    type: 'remote',
	    rPP: 20, 
	    rPPOptions:[20, 50, 100, 500],
	    layout: (isMobileDevice()) ? ["prev", "strPage", "next", "|", "strRpp", "|", "strDisplay"] : ["first","prev","|","strPage", "|","next","last","|","strRpp","|","refresh","|","strDisplay"],
		};
		
	  var selectionModel = {
	    type: 'row',
	    mode: 'single',
      toggle: false,
	    fireSelectChange: true
	  };
	
	  var scrollModel = {
      theme: true,
	  	autofit: false
	  };
	  
	  var dragModel = {
    	on: false
    };
    
    var dropModel = {
    	on: false
    };

  	$.paramquery.pqGrid.prototype.options.pageModel=pageModel;
  	$.paramquery.pqGrid.prototype.options.selectionModel=selectionModel;
  	$.paramquery.pqGrid.prototype.options.scrollModel=scrollModel;
  	$.paramquery.pqGrid.prototype.options.dragModel=dragModel;
  	$.paramquery.pqGrid.prototype.options.dropModel=dropModel;

	}
  
});

var holidays = [
	"01/01/2022", "04/15/2022", "04/18/2022",	"05/01/2022", "05/08/2022", "07/05/2022", "07/06/2022", "09/28/2022", "10/28/2022",	"11/17/2022", "12/24/2022", "12/25/2022", "12/26/2022",
	"01/01/2023", "04/07/2023", "04/10/2023",	"05/01/2023", "05/08/2023", "07/05/2023", "07/06/2023", "09/28/2023", "10/28/2023",	"11/17/2023", "12/24/2023", "12/25/2023", "12/26/2023",
	"01/01/2024", "03/28/2024", "04/01/2024",	"05/01/2023", "05/08/2023", "07/05/2023", "07/06/2023", "09/28/2023", "10/28/2023",	"11/17/2023", "12/24/2023", "12/25/2023", "12/26/2023",
];

function weekendOrHoliday(date) {
	// No Weekends
	var show = (date.getDay() != 0) && (date.getDay() != 6);
	for (var i = 0; i < holidays.length; i++) {
		if (new Date(holidays[i]).toString() == date.toString()) {
			show = false;
		}// No Holidays
	}

  return [true, (show) ? '' : 'ui-state-highlight', ''];
}
class DGrid {

  constructor (res, gridId, gridName, endpoint) {
    this.res=res;    
    this.gridId=gridId;
    this.gridName=gridName || 'grid';
    this.endpoint=endpoint;
    
    this.initEvents();    
  }

  initEvents() {
    const helper=this;

    //general event for hovering rows 
    $(helper.gridId).on('mouseenter', '.pq-grid-row', function(evt){
      $(this).find('.grid-icons').show();
    });
    
    //general event for hovering rows
    $(helper.gridId).on('mouseleave', '.pq-grid-row', function(evt){
      $(this).find('.grid-icons').hide();
    });
    
    //general event for quick search textbox
    $('#search').on('keyup', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      let text=$(this).val();
      let filter2={mode: 'OR', data:[]};
          
      for (let i=0; i<grid.colModel.length; i++) {
        if ((grid.colModel[i].filter) && (grid.colModel[i].dataType!='bool')) {
          grid.colModel[i].filter.value=text;
          filter2.data.push({dataIndx: grid.colModel[i].dataIndx, value: text, condition: grid.colModel[i].filter.condition});
        }  
      }
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};

      grid.refreshDataAndView();
    });
    
    //general event for excel export button
    $(document).on('click', '#gexcel', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      const format = 'xlsx';                            
      const blob = grid.exportData({
        format: format,        
        render: true
      });
      if (typeof blob === "string") {                            
        blob = new Blob([blob]);
      }
      saveAs(blob, helper.gridName + '.'+format );
    });

  }
  
  // xxx
  pqRenderMail(ui) {
	  let retval=ui.cellData;
	
	  if (retval) {
		  retval='<a href="mailto://'+ ui.cellData + '">{0}</a>';
	  }
	
	  return pqRenderFiltered(ui, retval);
  }

  // xxx
  pqRenderPhone(ui) {
  	let retval=ui.cellData;
  	
  	if (retval) {
  		retval='<a href="tel://'+ ui.cellData + '">{0}</a>';
  	}
  	
  	return pqRenderFiltered(ui, retval);
  }
  
  // xxx
  pqRenderBool(ui) {
  const value=ui.cellData;
    let html='';
    
    if (value) 
      html='<span class="fa fa-check" title="ano"></span>';
    else  
      html='<span class="fa fa-xmark" title="ne"></span>';
    
    return {
      text: html
    }
  }

  // xxx
  pqRenderAlert(ui) {
    const value=ui.cellData;
    let html='';
    
    switch (value) {
      case 1: html='<span class="fa fa-check" style="color: lightgreen;"></span>'; break;
      case 2: html='<span class="fa fa-exclamation-triangle" style="color: orange;"></span>'; break;
      case 3: html='<span class="fa fa-exclamation-triangle" style="color: red;"></span>'; break;
    }
    
    return {
      text: html
    }
  }

  // xxx
  pqRenderDateTime(ui) {
    const value=ui.cellData;
    let html='';
    
    if (value) {
      html=new Date(value).toLocaleString();
    }
    
    return {
      text: html
    }
  }

  // filterRender to highlight matching cell text.
  pqRenderFiltered(ui, customText) {
  	const filter = ui.column.filter;
  	let retval = ui.cellData;
  	
  	if (retval && filter && filter.value && !ui.Export) {
  		retval=retval.toString();
  		var condition = filter.condition, valUpper = retval.toUpperCase(), txt = filter.value, txt = (txt == null) ? "" : txt.toString(), txtUpper = txt.toUpperCase(), indx = -1;
  		if (condition == "end") {
  			indx = valUpper.lastIndexOf(txtUpper);
  			// if not at the end
  			if (indx + txtUpper.length != valUpper.length) {
  				indx = -1;
  			}
  		}
  		else if (condition == "contain") {
  			indx = valUpper.indexOf(txtUpper);
  		}
  		else if (condition == "begin") {
  			indx = valUpper.indexOf(txtUpper);
  			// if not at the beginning.
  			if (indx > 0) {
  				indx = -1;
  			}
  		}
  		if (indx >= 0) {
  			var txt1 = retval.substring(0, indx);
  			var txt2 = retval.substring(indx, indx + txt.length);
  			var txt3 = retval.substring(indx + txt.length);
  			retval = txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
  			if (customText)
  			  retval=customText.replace("{0}", retval);
  		}
  	}
  
  
  	return {
  		text : retval
  	};
  }
}
const MSG_WAIT                   = 0;
const MSG_OK                     = 1;
const MSG_WARNING                = 2;
const MSG_ERROR                  = 3;

class DControl {

  constructor () {
    this.initEvents();
  }

  initEvents() {
    const helper=this;

    //tristate onlick rolls state behavior including indeterminated
    $(document).on('click', '.tri-state', function(evt) {
      const control=$(this);
      
      if (control.data('state')=='none')
        control.data('state', 'checked').prop('checked', true).prop('indeterminate', false);
      else if (control.data('state')=='checked')    
        control.data('state', 'unchecked').prop('checked', false);
      else if (control.data('state')=='unchecked')    
        control.data('state', 'none').prop('indeterminate', true);
        
      evt.stopPropagation();  
    })
  }  
  
  setState(selector, state) {
    const helper=this;
    const control=$(selector);
    
    //in order to tristate works properly, we must set indeterminate programatically
    control.data('state', state);
    if (state=='none')
      control.prop('indeterminate', 'true');
  }
  
  dlgOpen(dlgname, path, params, callback) {
    const helper=this;

    $('.toasters').after('<div class="modal modal-right fade" id="' + dlgname + '" tabindex="-1" role="dialog" aria-hidden="true"></div>');
    $('#' + dlgname).load(cons.ctx + 'dialog/' + dlgname + path + '?jsoncallback=' + uuid(), function() {
  
      //call function to setup custom parameters
      setDlgParams(params, callback);
      
      //open modal
      $('#' + dlgname).modal('show');
    });
  }
  
  dlgClose(dlgname) {
    $('#' + dlgname).modal('hide');
    setTimeout(function () {
      $('#' + dlgname).remove();
    }, 500);
  }
  
  loaderShow(value) {
    $('.progress').show();
    loaderAdd(value);
  }
  
  loaderAdd(value) {
    var val=value;
    if (!value) {
      val=4;
    }
    var bar=$('.progress-bar');
    var width=parseInt(bar.attr('aria-valuenow'))+val;
    if (width>100)
      width=0;
    bar.attr('aria-valuenow', width);
    bar.width(width+'%');
    if (!value) {
      setTimeout(addLoader, 1000);
    }
  }
  
  loaderHide() {
    $('.progress').hide();
  }
  
  messageShow(type, timeout, msg) {
    var idx=$('.toasters').children().length+1;
    var cls='';
    var clr='';
    var txt='';
    switch (type) {
      case MSG_WAIT: cls='fa-spinner fa-spin'; break;
      case MSG_OK: cls='fa-check'; clr='lightgreen'; txt='Info'; break;
      case MSG_WARNING: cls='fa-exclamation-triangle'; clr='orange'; txt='Warning'; break;
      case MSG_ERROR: cls='fa-exclamation-triangle'; clr='red'; txt='Error'; timeout=9000; break;
    }
  
    var ahtml='';
    ahtml+='<div id="toast'+idx+'" class="toast" role="alert" aria-live="assertive" aria-atomic="true">';
    ahtml+='  <div class="toast-header">';
    ahtml+='    <span class="fa-solid '+cls+' mr-2" style="font-size: 20px; color: '+clr+';"></span>';
    ahtml+='    <strong class="mr-auto mt-1">'+txt+'</strong>';
    ahtml+='    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">';
    ahtml+='      <span aria-hidden="true" class="fa fa-xmark mt-1" style="font-size: 20px;"></span>';
    ahtml+='    </button>';
    ahtml+='  </div>';
    ahtml+='  <div class="toast-body text-wrap">';
    ahtml+='     <small>'+msg+'</small>';
    ahtml+='  </div>';
    ahtml+='</div>';
  
    $('.toasters').append(ahtml);
    
    var options={};
    if (timeout) {
      options.autohide=true;
      options.delay=timeout;
    }
    else {
      options.autohide=false;
    }
    $('#toast'+idx).toast(options);
    $('#toast'+idx).toast('show');
    
  }

  spinBtn(cls, btn) {
    let sel=(btn) ? btn+' .'+cls : '.'+cls;
    $(sel).removeClass(cls).addClass('fa-spinner fa-spin');
  }
  
  unspinBtn(cls, btn) {
    let sel=(btn) ? btn+' .fa-spin' : '.fa-spin';
    $(sel).removeClass('fa-spinner fa-spin').addClass(cls);
  }

}class DGridAlert extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {
    const helper=this;
    
    super.initEvents();
    
    $('#filter2Btn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#text').val();
      if (text)          
        filter2.data.push({dataIndx: 'alertText', value: text, condition: 'contain'});

      let info=$('#info');
      if (!info.prop('checked')) {
        filter2.data.push({dataIndx: 'alertType', value: '1', condition: 'equal'});
      }
      
      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#text').val('');

      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gdelete', function(evt){
      const id=$(this).data('id');
      console.log('delete alert '+id)
    });
    
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: '', width: 50, dataIndx: 'alertType', align: 'center', resizable: false, copy: false, render: this.pqRenderAlert,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.message, width: 250, dataIndx: 'alertMessage', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.text, width: 450, dataIndx: 'alertText', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.created, width: 160, dataIndx: 'createTs', align: 'right', resizable: false, render: this.pqRenderDateTime,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: res.expired, width: 160, dataIndx: 'expireTs', align: 'right', resizable: false, render: this.pqRenderDateTime, 
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: '', width: 60, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          html+='  <button id="gdelete" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fa-solid fa-trash" title="smazat"></span></button>';
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'createdTs', dir: 'down' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {        
        const pager=$(helper.gridId).find('.pq-grid-footer');
        let html = '<span id="gexcel" class="pq-ui-button ui-widget-header" style="cursor: pointer; background: #efdfff; border: 1px solid #af9fff;" tabindex="0" rel="tooltip" title="Excel"><span class="ui-icon ui-icon-disk"></span></span>';
        pager.prepend('<span class="pq-separator"></span>');
        pager.prepend(html);
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }
}
class DGridInstrument extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {

    super.initEvents();

    const helper=this;
        
    $('#filter2Btn').on('click', function(evt){      
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#acronym').val();
      if (text)          
        filter2.data.push({dataIndx: 'acronym', value: text, condition: 'contain'});

      let installed=$('#installed');
      if (!installed.prop('indeterminate')) {
        filter2.data.push({dataIndx: 'installed', value: ''+installed.prop('checked')+'', condition: 'equal'});
      }
      
      let measuring=$('#measuring');
      if (!measuring.prop('indeterminate')) {
        filter2.data.push({dataIndx: 'measuring', value: ''+installed.prop('checked')+'', condition: 'equal'});
      }
      
      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#acronym').val('');
      controls.setState('#installed', 'none');
      controls.setState('#measuring', 'none');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gedit', function(evt) {
      const id=$(this).data('id');
      controls.dlgOpen('dlg-instrument', '/'+id, {}, function() {
        console.log('saved');
      })
    });
    
    $(document).on('click', '#gmeasurements', function(evt) {
      const id=$(this).data('id');
      console.log('call measurements by instrumentid '+id)
    });
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res.department, width: 100, dataIndx: 'idepartmentAcronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.acronym, width: 150, dataIndx: 'acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.nameCZ, width: 150, dataIndx: 'nameCZ', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.installed, width: 50, dataIndx: 'installed', dataType: 'bool', align: 'center', resizable: false, copy: false, render: this.pqRenderBool,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: res.measuring, width: 50, dataIndx: 'measuring', dataType: 'bool', align: 'center', resizable: false, copy: false, render: this.pqRenderBool,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: '', width: 150, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          html+='  <button id="gedit" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-edit" title="edit"></span></button>';
          html+='  <button id="gmeasurements" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-scale-balanced" title="measurements"></span></button>';
          html+='  <button id="gusers" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-users" title="users"></span></button>';
          html+='  <button id="gbook" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-book" title="log"></span></button>';
          html+='  <button id="gactivate" data-id="'+ui.rowData.id+'" data-installed="'+ui.rowData.installed+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas '+((ui.rowData.installed) ? 'fa-stop' : 'fa-play')+'" title="'+((ui.rowData.installed) ? 'deactivate' : 'activate')+'"></span></button>';
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'idepartmentAcronym', dir: 'up' }, { dataIndx: 'acronym', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {        
        const pager=$(helper.gridId).find('.pq-grid-footer');
        let html = '<span id="gexcel" class="pq-ui-button ui-widget-header" style="cursor: pointer; background: #efdfff; border: 1px solid #af9fff;" tabindex="0" rel="tooltip" title="Excel"><span class="ui-icon ui-icon-disk"></span></span>';
        pager.prepend('<span class="pq-separator"></span>');
        pager.prepend(html);
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }  
}
class DGridDataset extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {

    super.initEvents();

    const helper=this;
        
    $('#filter2Btn').on('click', function(evt){      
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#acronym').val();
      if (text)          
        filter2.data.push({dataIndx: 'acronym', value: text, condition: 'contain'});

      let text2=$('#resource_internalNumber').val();
      if (text2)
        filter2.data.push({dataIndx: 'resource_internalNumber', value: text2, condition: 'contain'});

      let text3=$('#resource_shortcut').val();
      if (text3)
        filter2.data.push({dataIndx: 'resource_shortcut', value: text3, condition: 'contain'});

      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#acronym').val('');
      $('#resource_internalNumber').val('');
      $('#resource_shortcut').val('');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gclose', function(evt) {
      const id=$(this).data('id');
     console.log('close dataset by datasetId '+id)
    });
    
    $(document).on('click', '#gmeasurements', function(evt) {
      const id=$(this).data('id');
      console.log('open measurements by datasetId '+id)
    });

    $(document).on('click', '#gusers', function(evt) {
      const id=$(this).data('id');
      console.log('open users by datasetId '+id)
    });
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res.creation_date, width: 100, dataIndx: 'start', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.acronym, width: 150, dataIndx: 'acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.resource_number, width: 150, dataIndx: 'resource_internalNumber', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.resource_activity, width: 150, dataIndx: 'resource_activity', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.resource_acronym, width: 150, dataIndx: 'resource_shortcut', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: '', width: 150, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          html+='  <button id="gmeasurements" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-scale-balanced" title="measurements"></span></button>';
          html+='  <button id="gusers" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-users" title="users"></span></button>';
          if (ui.rowData.end == null) {
            html+='  <button id="gclose" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-xmark" title="close"></span></button>';
          }
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'start', dir: 'down' }, { dataIndx: 'acronym', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }  
}
class DGridMeasurement extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {

    super.initEvents();

    const helper=this;
        
    $('#filter2Btn').on('click', function(evt){      
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#acronym').val();
      if (text)          
        filter2.data.push({dataIndx: 'acronym', value: text, condition: 'contain'});

      let text2=$('#dataset_acronym').val();
      if (text2)
        filter2.data.push({dataIndx: 'dataset_acronym', value: text2, condition: 'contain'});


      let text3=$('#instrument_acronym').val();
      if (text3)
        filter2.data.push({dataIndx: 'instrument_acronym', value: text3, condition: 'contain'});

      let installed=$('#uploaded');
      if (!installed.prop('indeterminate')) {
         filter2.data.push({dataIndx: 'uploaded', value: ''+installed.prop('checked')+'', condition: 'equal'});
      }

      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#acronym').val('');
      $('#dataset_acronym').val('');
      $('#instrument_acronym').val('');
      controls.setState('#uploaded', 'none');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gclose', function(evt) {
      const id=$(this).data('id');
      controls.dlgOpen('dlg-measurement', '/'+id, {}, function() {
        console.log('closed');
      })
    });
    
    $(document).on('click', '#grepeat', function(evt) {
      const id=$(this).data('id');
      console.log('repeat measurement by measurementId '+id)
    });
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res.creation_date, width: 100, dataIndx: 'start', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.acronym, width: 150, dataIndx: 'acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.repetition, width: 150, dataIndx: 'id_repetition', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.dataset_singular, width: 150, dataIndx: 'dataset_acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.instrument_singular, width: 150, dataIndx: 'instrument_acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.uploaded, width: 50, dataIndx: 'uploaded', dataType: 'bool', align: 'center', resizable: false, copy: false, render: this.pqRenderBool,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }
      },
      { title: '', width: 150, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          if (ui.rowData.start == null) {
             html+='  <button id="gclose" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-xmark" title="close"></span></button>';
          } else {
             html+='  <button id="grepeat" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-repeat" title="repeat"></span></button>';
          }
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'start', dir: 'down' }, { dataIndx: 'acronym', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }  
}
class DGridS3Point extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 
    
    this.initGrid();
  }

  initEvents() {
    const helper=this;
    
    super.initEvents();
    
    $('#filter2Btn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#name').val();
      if (text)          
        filter2.data.push({dataIndx: 'name', value: text, condition: 'contain'});
      
      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    if (helper.gridId=='#grid1') {
    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#name').val('');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

     $(document).on('click', '#gdownload', function(evt) {
                const params = {bucketName: $(this).data('bucket'),
                                name: $(this).data('name'),
                                prefix: $(this).data('prefix')};
                const url='/action/s3object/download';
                $.ajax({
                              type: 'POST',
                              url: url,
                              cache: false,
                              contentType: 'application/json',
                              processData: true,
                              data: JSON.stringify(params),
                              success: function (data) {
                                    // Handle response data
                                    console.log(data);
                                    downloadFile(data);
                              },
                              error: function (error) {
                                controls.messageShow(MSG_ERROR, 25000, '<br><pre>'+error.status+', '+error.message+'</pre>');
                              }
                            })
        });

    $(document).on('click', '#gzip', function(evt) {
            const params = {bucketName: $(this).data('bucket'),
                            name: $(this).data('name'),
                            prefix: $(this).data('prefix')};
            controls.dlgOpen('dlg-zip', '', params, function() {
                console.log('downloaded');
            })
    });

    $(document).on('click', '#gsharetul', function(evt) {
            const params = {bucketName: $(this).data('bucket'),
                            name: $(this).data('name'),
                            prefix: $(this).data('prefix')};
            controls.dlgOpen('dlg-sharetul', '', params, function() {
                console.log('shared');
            })
    });

    $(document).on('click', '#gsharepub', function(evt) {
            const params = {bucketName: $(this).data('bucket'),
                            name: $(this).data('name'),
                            prefix: $(this).data('prefix')};
            controls.dlgOpen('dlg-sharepub', '', params, function() {
                console.log('shared');
            })
    });
    }
  }

  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res[helper.gridName], width: 150, dataIndx: 'name', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      }
    ];

    if (helper.gridId=='#grid2') {
        colM1.push({ title: '', width: 150, resizable: false, sortable: false, copy: false,
            render: function(ui) {
                let html='';
                html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
                if (ui.rowData.name.endsWith('/')) {
                     html+='  <button id="gzip" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download zip"></span></button>';
                } else {
                    html+='  <button id="gdownload" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download file"></span></button>';
                }
                html+='  <button id="gsharetul" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-share-nodes" title="share inside TUL"></span></button>';
                html+='  <button id="gsharepub" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-link" title="get temp public link"></span></button>';
                html+='</div>'
                return {
                  text: html
                }
            }});
   }

   if (helper.gridId=='#grid3') {
           colM1.push({ title: '', width: 150, resizable: false, sortable: false, copy: false,
               render: function(ui) {
                   let html='';
                   html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
                   if (ui.rowData.name.endsWith('/')) {
                        html+='  <button id="gzip" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download zip"></span></button>';
                   } else {
                       html+='  <button id="gdownload" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download file"></span></button>';
                   }
                   html+='  <button id="gsharepub" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-link" title="get temp public link"></span></button>';
                   html+='</div>'
                   return {
                     text: html
                   }
               }});
      }
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: '33%',
      height: '430',
      sortModel: {
        type: 'local',
        single: false,
        sorter: [{ dataIndx: 'name', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'local',
        mode: 'AND',
        header: true
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'local',
        rPP: 20,
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {        
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();

        if (helper.gridId=='#grid1') {
          const grid2=$('#grid2').pqGrid('instance');

          console.log('ready to call dataset api with bucket set to ', {name: ui.rowData.name})

          grid2.options.dataModel.postData={name: ui.rowData.name};
          grid2.refreshDataAndView();

          const grid3=$('#grid3').pqGrid('instance');
          grid3.options.dataModel.data=[];
          grid3.refreshView();
        } 
         
        if (helper.gridId=='#grid2' && ui.rowData.name.endsWith('/')) {
          const grid3=$('#grid3').pqGrid('instance');
          console.log('ready to call file api with dataset set to ', {bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name})

          grid3.options.dataModel.postData={bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name};
          grid3.refreshDataAndView();
        }

        if (helper.gridId=='#grid3' && ui.rowData.name.endsWith('/')) {
            const grid3=$('#grid3').pqGrid('instance');
            console.log('ready to call file api with object set to ', {bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name})

            grid3.options.dataModel.postData={bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name};
            grid3.refreshDataAndView();
        }
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }
}
