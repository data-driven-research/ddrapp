#!/bin/bash

# Delete old file if it exists
echo "------- deleting old file -------"
rm -f _ddrapp.js

# Concatenate JavaScript files
echo "----- compiling javascripts -----"
cat functions.js jquery.init.js DGrid.js DControl.js DGridAlert.js DGridInstrument.js DGridDataset.js DGridMeasurement.js DGridS3Point.js >> _ddrapp.js
