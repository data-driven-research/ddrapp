class DGridS3Point extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 
    
    this.initGrid();
  }

  initEvents() {
    const helper=this;
    
    super.initEvents();
    
    $('#filter2Btn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#name').val();
      if (text)          
        filter2.data.push({dataIndx: 'name', value: text, condition: 'contain'});
      
      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    if (helper.gridId=='#grid1') {
    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#name').val('');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

     $(document).on('click', '#gdownload', function(evt) {
                const params = {bucketName: $(this).data('bucket'),
                                name: $(this).data('name'),
                                prefix: $(this).data('prefix')};
                const url='/action/s3object/download';
                $.ajax({
                              type: 'POST',
                              url: url,
                              cache: false,
                              contentType: 'application/json',
                              processData: true,
                              data: JSON.stringify(params),
                              success: function (data) {
                                    // Handle response data
                                    console.log(data);
                                    downloadFile(data);
                              },
                              error: function (error) {
                                controls.messageShow(MSG_ERROR, 25000, '<br><pre>'+error.status+', '+error.message+'</pre>');
                              }
                            })
        });

    $(document).on('click', '#gzip', function(evt) {
            const params = {bucketName: $(this).data('bucket'),
                            name: $(this).data('name'),
                            prefix: $(this).data('prefix')};
            controls.dlgOpen('dlg-zip', '', params, function() {
                console.log('downloaded');
            })
    });

    $(document).on('click', '#gsharetul', function(evt) {
            const params = {bucketName: $(this).data('bucket'),
                            name: $(this).data('name'),
                            prefix: $(this).data('prefix')};
            controls.dlgOpen('dlg-sharetul', '', params, function() {
                console.log('shared');
            })
    });

    $(document).on('click', '#gsharepub', function(evt) {
            const params = {bucketName: $(this).data('bucket'),
                            name: $(this).data('name'),
                            prefix: $(this).data('prefix')};
            controls.dlgOpen('dlg-sharepub', '', params, function() {
                console.log('shared');
            })
    });
    }
  }

  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res[helper.gridName], width: 150, dataIndx: 'name', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      }
    ];

    if (helper.gridId=='#grid2') {
        colM1.push({ title: '', width: 150, resizable: false, sortable: false, copy: false,
            render: function(ui) {
                let html='';
                html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
                if (ui.rowData.name.endsWith('/')) {
                     html+='  <button id="gzip" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download zip"></span></button>';
                } else {
                    html+='  <button id="gdownload" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download file"></span></button>';
                }
                html+='  <button id="gsharetul" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-share-nodes" title="share inside TUL"></span></button>';
                html+='  <button id="gsharepub" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-link" title="get temp public link"></span></button>';
                html+='</div>'
                return {
                  text: html
                }
            }});
   }

   if (helper.gridId=='#grid3') {
           colM1.push({ title: '', width: 150, resizable: false, sortable: false, copy: false,
               render: function(ui) {
                   let html='';
                   html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
                   if (ui.rowData.name.endsWith('/')) {
                        html+='  <button id="gzip" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download zip"></span></button>';
                   } else {
                       html+='  <button id="gdownload" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-download" title="download file"></span></button>';
                   }
                   html+='  <button id="gsharepub" data-bucket="'+ui.rowData.bucketName+'" data-name="'+ui.rowData.name+'" data-prefix="'+ui.rowData.prefix+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-link" title="get temp public link"></span></button>';
                   html+='</div>'
                   return {
                     text: html
                   }
               }});
      }
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: '33%',
      height: '430',
      sortModel: {
        type: 'local',
        single: false,
        sorter: [{ dataIndx: 'name', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'local',
        mode: 'AND',
        header: true
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'local',
        rPP: 20,
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {        
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();

        if (helper.gridId=='#grid1') {
          const grid2=$('#grid2').pqGrid('instance');

          console.log('ready to call dataset api with bucket set to ', {name: ui.rowData.name})

          grid2.options.dataModel.postData={name: ui.rowData.name};
          grid2.refreshDataAndView();

          const grid3=$('#grid3').pqGrid('instance');
          grid3.options.dataModel.data=[];
          grid3.refreshView();
        } 
         
        if (helper.gridId=='#grid2' && ui.rowData.name.endsWith('/')) {
          const grid3=$('#grid3').pqGrid('instance');
          console.log('ready to call file api with dataset set to ', {bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name})

          grid3.options.dataModel.postData={bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name};
          grid3.refreshDataAndView();
        }

        if (helper.gridId=='#grid3' && ui.rowData.name.endsWith('/')) {
            const grid3=$('#grid3').pqGrid('instance');
            console.log('ready to call file api with object set to ', {bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name})

            grid3.options.dataModel.postData={bucketName: ui.rowData.bucketName, prefix: ui.rowData.prefix, name: ui.rowData.name};
            grid3.refreshDataAndView();
        }
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }
}
