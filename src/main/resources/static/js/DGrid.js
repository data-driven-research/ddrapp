class DGrid {

  constructor (res, gridId, gridName, endpoint) {
    this.res=res;    
    this.gridId=gridId;
    this.gridName=gridName || 'grid';
    this.endpoint=endpoint;
    
    this.initEvents();    
  }

  initEvents() {
    const helper=this;

    //general event for hovering rows 
    $(helper.gridId).on('mouseenter', '.pq-grid-row', function(evt){
      $(this).find('.grid-icons').show();
    });
    
    //general event for hovering rows
    $(helper.gridId).on('mouseleave', '.pq-grid-row', function(evt){
      $(this).find('.grid-icons').hide();
    });
    
    //general event for quick search textbox
    $('#search').on('keyup', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      let text=$(this).val();
      let filter2={mode: 'OR', data:[]};
          
      for (let i=0; i<grid.colModel.length; i++) {
        if ((grid.colModel[i].filter) && (grid.colModel[i].dataType!='bool')) {
          grid.colModel[i].filter.value=text;
          filter2.data.push({dataIndx: grid.colModel[i].dataIndx, value: text, condition: grid.colModel[i].filter.condition});
        }  
      }
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};

      grid.refreshDataAndView();
    });
    
    //general event for excel export button
    $(document).on('click', '#gexcel', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      const format = 'xlsx';                            
      const blob = grid.exportData({
        format: format,        
        render: true
      });
      if (typeof blob === "string") {                            
        blob = new Blob([blob]);
      }
      saveAs(blob, helper.gridName + '.'+format );
    });

  }
  
  // xxx
  pqRenderMail(ui) {
	  let retval=ui.cellData;
	
	  if (retval) {
		  retval='<a href="mailto://'+ ui.cellData + '">{0}</a>';
	  }
	
	  return pqRenderFiltered(ui, retval);
  }

  // xxx
  pqRenderPhone(ui) {
  	let retval=ui.cellData;
  	
  	if (retval) {
  		retval='<a href="tel://'+ ui.cellData + '">{0}</a>';
  	}
  	
  	return pqRenderFiltered(ui, retval);
  }
  
  // xxx
  pqRenderBool(ui) {
  const value=ui.cellData;
    let html='';
    
    if (value) 
      html='<span class="fa fa-check" title="ano"></span>';
    else  
      html='<span class="fa fa-xmark" title="ne"></span>';
    
    return {
      text: html
    }
  }

  // xxx
  pqRenderAlert(ui) {
    const value=ui.cellData;
    let html='';
    
    switch (value) {
      case 1: html='<span class="fa fa-check" style="color: lightgreen;"></span>'; break;
      case 2: html='<span class="fa fa-exclamation-triangle" style="color: orange;"></span>'; break;
      case 3: html='<span class="fa fa-exclamation-triangle" style="color: red;"></span>'; break;
    }
    
    return {
      text: html
    }
  }

  // xxx
  pqRenderDateTime(ui) {
    const value=ui.cellData;
    let html='';
    
    if (value) {
      html=new Date(value).toLocaleString();
    }
    
    return {
      text: html
    }
  }

  // filterRender to highlight matching cell text.
  pqRenderFiltered(ui, customText) {
  	const filter = ui.column.filter;
  	let retval = ui.cellData;
  	
  	if (retval && filter && filter.value && !ui.Export) {
  		retval=retval.toString();
  		var condition = filter.condition, valUpper = retval.toUpperCase(), txt = filter.value, txt = (txt == null) ? "" : txt.toString(), txtUpper = txt.toUpperCase(), indx = -1;
  		if (condition == "end") {
  			indx = valUpper.lastIndexOf(txtUpper);
  			// if not at the end
  			if (indx + txtUpper.length != valUpper.length) {
  				indx = -1;
  			}
  		}
  		else if (condition == "contain") {
  			indx = valUpper.indexOf(txtUpper);
  		}
  		else if (condition == "begin") {
  			indx = valUpper.indexOf(txtUpper);
  			// if not at the beginning.
  			if (indx > 0) {
  				indx = -1;
  			}
  		}
  		if (indx >= 0) {
  			var txt1 = retval.substring(0, indx);
  			var txt2 = retval.substring(indx, indx + txt.length);
  			var txt3 = retval.substring(indx + txt.length);
  			retval = txt1 + "<span style='background:yellow;color:#333;'>" + txt2 + "</span>" + txt3;
  			if (customText)
  			  retval=customText.replace("{0}", retval);
  		}
  	}
  
  
  	return {
  		text : retval
  	};
  }
}
