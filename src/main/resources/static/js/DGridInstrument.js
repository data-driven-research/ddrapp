class DGridInstrument extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {

    super.initEvents();

    const helper=this;
        
    $('#filter2Btn').on('click', function(evt){      
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#acronym').val();
      if (text)          
        filter2.data.push({dataIndx: 'acronym', value: text, condition: 'contain'});

      let installed=$('#installed');
      if (!installed.prop('indeterminate')) {
        filter2.data.push({dataIndx: 'installed', value: ''+installed.prop('checked')+'', condition: 'equal'});
      }
      
      let measuring=$('#measuring');
      if (!measuring.prop('indeterminate')) {
        filter2.data.push({dataIndx: 'measuring', value: ''+installed.prop('checked')+'', condition: 'equal'});
      }
      
      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#acronym').val('');
      controls.setState('#installed', 'none');
      controls.setState('#measuring', 'none');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gedit', function(evt) {
      const id=$(this).data('id');
      controls.dlgOpen('dlg-instrument', '/'+id, {}, function() {
        console.log('saved');
      })
    });
    
    $(document).on('click', '#gmeasurements', function(evt) {
      const id=$(this).data('id');
      console.log('call measurements by instrumentid '+id)
    });
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res.department, width: 100, dataIndx: 'idepartmentAcronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.acronym, width: 150, dataIndx: 'acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.nameCZ, width: 150, dataIndx: 'nameCZ', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.installed, width: 50, dataIndx: 'installed', dataType: 'bool', align: 'center', resizable: false, copy: false, render: this.pqRenderBool,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: res.measuring, width: 50, dataIndx: 'measuring', dataType: 'bool', align: 'center', resizable: false, copy: false, render: this.pqRenderBool,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: '', width: 150, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          html+='  <button id="gedit" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-edit" title="edit"></span></button>';
          html+='  <button id="gmeasurements" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-scale-balanced" title="measurements"></span></button>';
          html+='  <button id="gusers" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-users" title="users"></span></button>';
          html+='  <button id="gbook" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-book" title="log"></span></button>';
          html+='  <button id="gactivate" data-id="'+ui.rowData.id+'" data-installed="'+ui.rowData.installed+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas '+((ui.rowData.installed) ? 'fa-stop' : 'fa-play')+'" title="'+((ui.rowData.installed) ? 'deactivate' : 'activate')+'"></span></button>';
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'idepartmentAcronym', dir: 'up' }, { dataIndx: 'acronym', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {        
        const pager=$(helper.gridId).find('.pq-grid-footer');
        let html = '<span id="gexcel" class="pq-ui-button ui-widget-header" style="cursor: pointer; background: #efdfff; border: 1px solid #af9fff;" tabindex="0" rel="tooltip" title="Excel"><span class="ui-icon ui-icon-disk"></span></span>';
        pager.prepend('<span class="pq-separator"></span>');
        pager.prepend(html);
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }  
}
