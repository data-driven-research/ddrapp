function isMobileDevice() {
  var mobileRegEx = new RegExp("Android|webOS|iPhone|iPad|iPod|BlackBerry", "i");
  return mobileRegEx.test(navigator.userAgent);
}

function setSizes(v, h) {
  var dv = $(window).width();
  if (dv > v) dv=v;
  var dh = $(window).height();
  if (dh > h) dh=h;
  return {width: dv, height: dh};
}

function uuid() {
	var tstmp = new Date();
	return tstmp.getTime();
}

function downloadAndZip(data) {
        const zip = new JSZip();
        const frontendDomain = window.location.origin;
        console.log(frontendDomain);
        const promises = data.fileUrls.map(async fileUrl => {
            try {
                const filename = fileUrl.prefix + fileUrl.url.substring(fileUrl.url.lastIndexOf('/') + 1, fileUrl.url.indexOf('?'));
                const response = await fetch(fileUrl.url, {
                                                    method: 'GET',
                                                    mode: 'cors',
                                                    cache: 'no-cache',
                                                    headers: {
                                                       Origin: window.location.origin,
                                                    },
                                                  });
                if (!response.ok) {
                    throw new Error(`Failed to fetch ${fileUrl.url}: ${response.statusText}`);
                }
                const blob = await response.blob();
                if (!blob.size) {
                    throw new Error(`Failed to create blob for ${fileUrl.url}`);
                }
                zip.file(filename, blob, { createFolders: true });
            } catch (error) {
                console.error(error);
                throw error; // Rethrow error to catch it later
            }
        });

        // Use Promise.all to wait for all promises to resolve
        Promise.all(promises).then(() => {
            zip.generateAsync({ type: 'blob' }).then(content => {
                const blobUrl = URL.createObjectURL(content);
                const link = document.createElement('a');
                link.href = blobUrl;
                link.download = 's3dataset.zip';
                link.click();
                URL.revokeObjectURL(blobUrl);
            }).catch(error => {
                console.error('Failed to generate zip file:', error);
            });
        }).catch(error => {
            console.error('Failed to add files to zip:', error);
        });
}

function downloadFile(data) {
        const frontendDomain = window.location.origin;
        console.log(frontendDomain);
        const url = data.url

        const filename = url.substring(url.lastIndexOf('/') + 1, url.indexOf('?'));
        fetch(url, {
            method: 'GET',
            mode: 'cors',
            cache: 'no-cache',
            headers: {
                 Origin: window.location.origin,
            },
            }).then(response => {
                    if (!response.ok) {
                        throw new Error(`Failed to fetch ${url}: ${response.statusText}`);
                    }
                    return response.blob();
                }).then(blob => {
                    if (!blob.size) {
                        throw new Error(`Failed to create blob for ${url}`);
                    }
                    const blobUrl = URL.createObjectURL(blob);
                    const link = document.createElement('a');
                    link.href = blobUrl;
                    link.download = filename;
                    link.click();
                    URL.revokeObjectURL(blobUrl);
                }).catch(error => {
                    console.error('Failed to download file:', error);
        });
}

