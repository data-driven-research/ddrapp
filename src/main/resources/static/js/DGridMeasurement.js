class DGridMeasurement extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {

    super.initEvents();

    const helper=this;
        
    $('#filter2Btn').on('click', function(evt){      
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#acronym').val();
      if (text)          
        filter2.data.push({dataIndx: 'acronym', value: text, condition: 'contain'});

      let text2=$('#dataset_acronym').val();
      if (text2)
        filter2.data.push({dataIndx: 'dataset_acronym', value: text2, condition: 'contain'});


      let text3=$('#instrument_acronym').val();
      if (text3)
        filter2.data.push({dataIndx: 'instrument_acronym', value: text3, condition: 'contain'});

      let installed=$('#uploaded');
      if (!installed.prop('indeterminate')) {
         filter2.data.push({dataIndx: 'uploaded', value: ''+installed.prop('checked')+'', condition: 'equal'});
      }

      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#acronym').val('');
      $('#dataset_acronym').val('');
      $('#instrument_acronym').val('');
      controls.setState('#uploaded', 'none');
      
      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gclose', function(evt) {
      const id=$(this).data('id');
      controls.dlgOpen('dlg-measurement', '/'+id, {}, function() {
        console.log('closed');
      })
    });
    
    $(document).on('click', '#grepeat', function(evt) {
      const id=$(this).data('id');
      console.log('repeat measurement by measurementId '+id)
    });
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: res.creation_date, width: 100, dataIndx: 'start', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.acronym, width: 150, dataIndx: 'acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.repetition, width: 150, dataIndx: 'id_repetition', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.dataset_singular, width: 150, dataIndx: 'dataset_acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.instrument_singular, width: 150, dataIndx: 'instrument_acronym', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.uploaded, width: 50, dataIndx: 'uploaded', dataType: 'bool', align: 'center', resizable: false, copy: false, render: this.pqRenderBool,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }
      },
      { title: '', width: 150, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          if (ui.rowData.start == null) {
             html+='  <button id="gclose" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-xmark" title="close"></span></button>';
          } else {
             html+='  <button id="grepeat" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fas fa-repeat" title="repeat"></span></button>';
          }
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'start', dir: 'down' }, { dataIndx: 'acronym', dir: 'up' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }  
}
