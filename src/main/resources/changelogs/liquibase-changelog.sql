-- liquibase formatted sql

-- changeset ddr:addEInfraIDColumn
ALTER TABLE users ADD COLUMN e_infra_id TEXT;
-- changeset ddr:dropACLDataset
ALTER TABLE user_dataset DROP COLUMN access_level;
-- changeset ddr:dropACLInstrument
ALTER TABLE user_instrument DROP COLUMN access_level;
-- changeset ddr:addCardIDColumn
ALTER TABLE users ADD COLUMN card_id TEXT;
-- changeset ddr:createTheAlertsTable
CREATE TABLE alerts (
    id BIGSERIAL PRIMARY KEY,
    alert_type BIGINT,
    alert_message TEXT,
    alert_text TEXT,
    user_email TEXT NOT NULL,
    create_ts TIMESTAMP NOT NULL,
    expire_ts TIMESTAMP,
    CONSTRAINT FK_alerts_users FOREIGN KEY (user_email) REFERENCES users(email)
);
-- changeset ddr:addTenantColumn
ALTER TABLE users ADD COLUMN tenant BIGINT;
-- changeset ddr:addLangColumn
ALTER TABLE users ADD COLUMN lang TEXT;

