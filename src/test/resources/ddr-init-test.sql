CREATE TABLE "users"
(
    "email"      text PRIMARY KEY,
    "department" text DEFAULT 'GUEST',
    "card_id"    text,
    "role"       text DEFAULT 'GUEST'
);

CREATE TABLE "resources"
(
    "id"              bigint PRIMARY KEY,
    "name_cz"         text NOT NULL,
    "name_en"         text NOT NULL,
    "internal_number" text NOT NULL,
    "activity"        text NOT NULL,
    "department"      text,
    "program"         text,
    "acronym"         text NOT NULL,
    "type"            text NOT NULL,
    "data_path"       text UNIQUE
);

CREATE TABLE "user_dataset"
(
    "user_email"   text,
    "dataset_id"   uuid,
    "access_level" text DEFAULT 'R',
    PRIMARY KEY ("user_email", "dataset_id")
);

CREATE TABLE "datasets"
(
    "id"          uuid   PRIMARY KEY,
    "name_cz"     text      NOT NULL,
    "name_en"     text      NOT NULL,
    "acronym"     text      NOT NULL,
    "resource_id" bigint    NOT NULL,
    "start_ts"    timestamp NOT NULL,
    "end_ts"      timestamp,
    "data_path"   text UNIQUE
);

CREATE TABLE "measurements"
(
    "id"              uuid,
    "repetition"      int,
    "acronym"         text      NOT NULL,
    "user_email"      text      NOT NULL,
    "dataset_id"      uuid      NOT NULL,
    "instrument_id"   text      NOT NULL,
    "start_ts"        timestamp NOT NULL,
    "end_ts"          timestamp,
    "data_path"       text,
    "uploaded"        boolean,
    "custom_metadata" jsonb DEFAULT '{}',
    PRIMARY KEY ("id", "repetition")
);

CREATE TABLE "user_instrument"
(
    "user_email"    text,
    "instrument_id" text,
    "access_level"  text DEFAULT 'R',
    PRIMARY KEY ("user_email", "instrument_id")
);

CREATE TABLE "instruments"
(
    "id"                text PRIMARY KEY,
    "name_cz"           text        NOT NULL,
    "name_en"           text        NOT NULL,
    "acronym"           text        NOT NULL,
    "description_cz"    text,
    "description_en"    text,
    "image_url"         text UNIQUE NOT NULL,
    "measuring"         boolean     NOT NULL,
    "installed"         boolean     NOT NULL,
    "department"        text        NOT NULL,
    "registration_info" jsonb       NOT NULL
);

ALTER TABLE "user_dataset"
    ADD FOREIGN KEY ("user_email") REFERENCES "users" ("email");

ALTER TABLE "user_dataset"
    ADD FOREIGN KEY ("dataset_id") REFERENCES "datasets" ("id");

ALTER TABLE "datasets"
    ADD FOREIGN KEY ("resource_id") REFERENCES "resources" ("id");

ALTER TABLE "measurements"
    ADD FOREIGN KEY ("user_email") REFERENCES "users" ("email");

ALTER TABLE "measurements"
    ADD FOREIGN KEY ("dataset_id") REFERENCES "datasets" ("id");

ALTER TABLE "measurements"
    ADD FOREIGN KEY ("instrument_id") REFERENCES "instruments" ("id");

ALTER TABLE "user_instrument"
    ADD FOREIGN KEY ("user_email") REFERENCES "users" ("email");

ALTER TABLE "user_instrument"
    ADD FOREIGN KEY ("instrument_id") REFERENCES "instruments" ("id");


INSERT INTO "users" ("email", "department", "card_id", "role")
VALUES
    ('test.user@tul.cz', 'OMI', '555', 'ADMINISTRATOR'),
    ('jane.smith@example.com', 'ONCH', '534', 'OPERATOR'),
    ('guest@example.com', 'GUEST', '821', 'GUEST');

INSERT INTO "resources" ("id", "name_cz", "name_en", "internal_number", "activity", "department", "program", "acronym", "type", "data_path")
VALUES
    (1, 'Resource 1 CZ', 'Resource 1 EN', 'INT-001', 'Active', 'Department A', 'Program 1', 'R1', 'Type A', '/data/resource1'),
    (2, 'Resource 2 CZ', 'Resource 2 EN', 'INT-002', 'Inactive', 'Department B', 'Program 2', 'R2', 'Type B', '/data/resource2');

INSERT INTO "datasets" ("id", "name_cz", "name_en", "acronym", "resource_id", "start_ts", "end_ts", "data_path")
VALUES
    ('ae45f48e-978d-4b01-8649-1ef87d645347', 'Dataset 1 CZ', 'Dataset 1 EN', 'DS1', 1, '2023-07-01 00:00:00', null, '/data/dataset1'),
    ('da2f710d-cc22-4823-8a2c-13620dd5036c', 'Dataset 2 CZ', 'Dataset 2 EN', 'DS2', 2, '2023-08-01 00:00:00', '2023-08-31 23:59:59', '/data/dataset2');

INSERT INTO "user_dataset" ("user_email", "dataset_id")
VALUES
    ('test.user@tul.cz', 'ae45f48e-978d-4b01-8649-1ef87d645347'),
    ('jane.smith@example.com', 'da2f710d-cc22-4823-8a2c-13620dd5036c');

INSERT INTO "instruments" ("id", "name_cz", "name_en", "acronym", "description_cz", "description_en", "image_url", "measuring", "installed", "department", "registration_info")
VALUES
    ('I1', 'Instrument 1 CZ', 'Instrument 1 EN', 'Ins1', 'Popis nástroje 1', 'Description of Instrument 1', 'https://example.com/instrument1.jpg', false, true, 'OMI', '{"serial_number": "SN123", "manufacturer": "ABC Inc."}'),
    ('I2', 'Instrument 2 CZ', 'Instrument 2 EN', 'Ins2', 'Popis nástroje 2', 'Description of Instrument 2', 'https://example.com/instrument2.jpg', false, true, 'ONCH', '{"serial_number": "SN456", "manufacturer": "XYZ Corp."}');

INSERT INTO "user_instrument" ("user_email", "instrument_id")
VALUES
    ('test.user@tul.cz', 'I1'),
    ('jane.smith@example.com', 'I2');

INSERT INTO "measurements" ("id", "repetition", "acronym", "user_email", "dataset_id", "instrument_id", "start_ts", "end_ts", "data_path", "uploaded", "custom_metadata")
VALUES
    ('bf85742d-91ac-4c6b-91a7-3832f69f4a7b', 1, 'M1', 'test.user@tul.cz', 'ae45f48e-978d-4b01-8649-1ef87d645347', 'I1', '2023-07-10 10:00:00', null, '/data/measurement1', false, '{"param1": 42, "param2": "abc"}'),
    ('9f5b3949-90e8-4e8c-bca0-6c6d8bf256b4', 1, 'M2', 'jane.smith@example.com', 'da2f710d-cc22-4823-8a2c-13620dd5036c', 'I2', '2023-08-05 15:00:00', '2023-08-05 16:30:00', '/data/measurement2', true, '{"param1": 23.5, "param2": "xyz"}');
