package cz.tul.cxi.ddr.controller;

import cz.tul.cxi.ddr.AbstractContainerBase;
import cz.tul.cxi.ddr.DdrWebApplication;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = DdrWebApplication.class)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ControllerTest extends AbstractContainerBase {
    @Autowired
    private MockMvc mvc;

    @Test
    @Order(1)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testConsole() {
        System.out.println("test console and detail");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/detail/user/test.user@tul.cz")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    @Test
    @Order(2)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testRegistrationForms() {
        System.out.println("test getting of registration forms");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/registration/instrument")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/registration/dataset")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/registration/resource")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/registration/measurement")
                            .param("byEntity", "user")
                            .param("byId", "test.user@tul.cz")
                            .param("byAcronym", "test.user@tul.cz")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    @Test
    @Order(3)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testLists() {
        System.out.println("test getting of lists");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/datasets")
                            .param("byEntity", "user")
                            .param("byId", "test.user@tul.cz")
                            .param("byAcronym", "test.user@tul.cz")
                            .param("activeOnly", "false")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/instruments")
                            .param("byEntity", "user")
                            .param("byId", "test.user@tul.cz")
                            .param("byAcronym", "test.user@tul.cz")
                            .param("availableOnly", "false")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/measurements")
                            .param("byEntity", "dataset")
                            .param("byId", "ae45f48e-978d-4b01-8649-1ef87d645347")
                            .param("byAcronym", "DS1")
                            .param("activeOnly", "false")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/measurements")
                            .param("byEntity", "instrument")
                            .param("byId", "I1")
                            .param("byAcronym", "Ins1")
                            .param("activeOnly", "false")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/measurements")
                            .param("byEntity", "user")
                            .param("byId", "test.user@tul.cz")
                            .param("byAcronym", "test.user@tul.cz")
                            .param("activeOnly", "false")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/users")
                            .param("byEntity", "dataset")
                            .param("byId", "ae45f48e-978d-4b01-8649-1ef87d645347")
                            .param("byAcronym", "DS1")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/list/users")
                            .param("byEntity", "instrument")
                            .param("byId", "I1")
                            .param("byAcronym", "Ins1")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    @Test
    @Order(4)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testActions() {
        System.out.println("test performing of actions");
        try {
            System.out.println("test performing of actions 1");
            mvc.perform(MockMvcRequestBuilders
                            .get("/action/measurement/bf85742d-91ac-4c6b-91a7-3832f69f4a7b/1/end")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            // needs airflow dependency
            System.out.println("test performing of actions 2");
            mvc.perform(MockMvcRequestBuilders
                            .post("/action/measurement/end")
                            .param("id.measurementId", "bf85742d-91ac-4c6b-91a7-3832f69f4a7b")
                            .param("id.repetition", "1")
                            .param("end", "2023-11-11 00:00:10")
                            .param("acronym", "M1")
                            .param("dataset", "ae45f48e-978d-4b01-8649-1ef87d645347")
                            .param("datasetAcronym", "DS1")
                            .param("instrument", "I1")
                            .param("instrumentAcronym", "Ins1")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().is3xxRedirection());
            System.out.println("test performing of actions 3");
            mvc.perform(MockMvcRequestBuilders
                            .get("/action/dataset/ae45f48e-978d-4b01-8649-1ef87d645347/close")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().is3xxRedirection());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    @Test
    @Order(5)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testModifications() {
        System.out.println("test performing of modifications");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/modification/dataset/ae45f48e-978d-4b01-8649-1ef87d645347/addUser")
                            .param("entityAcronym", "DS1")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/modification/instrument/I1/addUser")
                            .param("entityAcronym", "Ins1")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/modification/instrument/I1/removeUser/guest@example.com")
                            .param("entityAcronym", "Ins1")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().is3xxRedirection());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    @Test
    @Order(6)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testError() {
        System.out.println("tests error handling");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/non-existent")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isNotFound());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }
}
