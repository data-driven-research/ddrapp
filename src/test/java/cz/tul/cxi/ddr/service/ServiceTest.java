package cz.tul.cxi.ddr.service;

import cz.tul.cxi.ddr.AbstractContainerBase;
import cz.tul.cxi.ddr.model.postgres.CompositeMeasurementId;
import cz.tul.cxi.ddr.model.auxiliary.DepartmentAcronym;
import cz.tul.cxi.ddr.model.auxiliary.UserRole;
import cz.tul.cxi.ddr.model.postgres.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ServiceTest extends AbstractContainerBase {
    private static User user;
    private static Resource resource;
    private static Dataset dataset;
    private static Instrument instrument;
    private static Measurement measurement;
    @Autowired
    private UserService userService;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private MeasurementService measurementService;
    @Autowired
    private DatasetService datasetService;
    @Autowired
    private InstrumentService instrumentService;

    @BeforeAll
    public static void initObjects() {
        user = new User();
        user.setEmail("test.user@tul.cz");
        user.setUDepartmentAcronym(DepartmentAcronym.OMI);
        user.setCardID("555");
        user.setRole(UserRole.ADMINISTRATOR);

        resource = new Resource();
        resource.setId(156L);
        resource.setInternalNumber("Resource 1");
        resource.setDepartment("111");
        resource.setActivity("568");
        resource.setType("Zakázka");
        resource.setNameCz("Název zdroje 1");
        resource.setNameEn("Name of resource 1");
        resource.setShortcut("Res1");

        dataset = new Dataset();
        dataset.setId(UUID.randomUUID());
        dataset.setResource(resource);
        dataset.setNameCZ("Popis 1");
        dataset.setNameEN("Description 1");
        dataset.setAcronym("Ds1");

        instrument = new Instrument();
        instrument.setId("54332");
        instrument.setNameCZ("Jméno na web 1");
        instrument.setNameEN("Name for the web 1");
        instrument.setAcronym("Instr1");
        instrument.setDescriptionCZ("Popis 1");
        instrument.setDescriptionEN("Description 1");
        instrument.setImageURL("url/image");
        instrument.setIdepartmentAcronym(DepartmentAcronym.GUEST);

        measurement = new Measurement();
        measurement.setId(new CompositeMeasurementId(UUID.randomUUID(), 1));
        measurement.setAcronym("Measurement1");
        measurement.setInstrument(instrument);
        measurement.setDataset(dataset);
        measurement.setUser(user);
    }

    @Test
    @Order(1)
    public void testSaveOfUser() {
        System.out.println("Test save of user");
        userService.save(user);
    }

    @Test
    @Order(2)
    public void testSaveOfResource() {
        resourceService.save(resource);
    }

    @Test
    @Order(3)
    public void testSaveOfDataset() {
        datasetService.saveDataset(user, dataset);
    }

    @Test
    @Order(4)
    public void testSaveOfInstrument() {
        instrumentService.saveInstrument(user, instrument, "image/path");
    }

    @Test
    @Order(5)
    public void testStartOfMeasurement() {
        assertThrows(IllegalStateException.class,
                () -> measurementService.startMeasurement(measurement, user, false));
        instrumentService.setInstalled(instrument.getId(), true);
        measurement.getInstrument().setInstalled(true);
        measurementService.startMeasurement(measurement, user, false);
    }

    @Test
    @Order(6)
    public void testFailCloseOfDataset() {
        assertThrows(IllegalStateException.class,
                () -> datasetService.closeDataset(dataset.getId()));
    }

    @Test
    @Order(7)
    public void testEndOfMeasurement() {
        measurement.setCustomMetadata(
                new HashMap<>(Map.of("tags", new ArrayList<>(List.of("tag1", "tag2")))));
        measurement.setEnd(Timestamp.from(Instant.now(Clock.system(ZoneId.of("Europe/Prague")))));
        measurementService.endMeasurement(measurement, user);
    }

    @Test
    @Order(8)
    public void testCloseOfDataset() {
        datasetService.closeDataset(dataset.getId());
    }
}
