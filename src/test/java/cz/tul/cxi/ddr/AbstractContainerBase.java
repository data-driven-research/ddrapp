package cz.tul.cxi.ddr;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

public abstract class AbstractContainerBase {

    static final ElasticsearchContainer ELASTICSEARCH_CONTAINER;

    static final PostgreSQLContainer<?> POSTGRE_SQL_CONTAINER;

    static {
        ELASTICSEARCH_CONTAINER = new ElasticsearchContainer(
                "docker.elastic.co/elasticsearch/elasticsearch:8.7.1")
                .withEnv("discovery.type", "single-node")
                .withEnv("xpack.security.enabled", "false")
                .withExposedPorts(9200)
                .withCreateContainerCmdModifier(cmd -> cmd.withHostConfig(
                        new HostConfig().withPortBindings(
                                new PortBinding(Ports.Binding.bindPort(34343), new ExposedPort(9200)))
                ));
        ELASTICSEARCH_CONTAINER.setWaitStrategy(new LogMessageWaitStrategy()
                .withRegEx(".* \"log.level\": \"INFO\", \"message\":\"started.*"));
        ELASTICSEARCH_CONTAINER.start();

        POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(PostgreSQLContainer.IMAGE);
        POSTGRE_SQL_CONTAINER.start();
    }
}
