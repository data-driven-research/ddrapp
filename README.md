# DDR - Data driven research

https://ddr.cxi.tul.cz (available only at TUL)  
http://147.230.187.179 (public demo instance)

## Description

Web application developed within the CESNET project DDR - Data-driven research implemented at CXI TUL. The backbone of
the web application is the Java framework SpringBoot. As a repository of data needed for the application is used
PostgreSQL and Elasticsearch. The application is built using Gradle, containerized using Docker, and deployed using
Docker Compose. More in the [wiki](../../../wikis/home).

## Deployment

### DDRapp

Web applications can be deployed using Docker Compose. You need to configure the access for the database, set up the
Shibboleth Service Provider metadata, generate and let sign up the app certificate, get and access keys to S3, and
provide the path to all necessary certificates and keys. The process is described as comments in the source code in
the *deploy* directory. All configuration needed to be set has their example value marked by <<>>.

To deploy the application to your server, you must copy the contents of the *deploy* directory to your target
directory, with all the necessary information and paths set in *deploy/docker-compose.yaml*,
*deploy/config/application.properties*,
and *deploy/.env*, and all the required certificates and keys moved to the *deploy/keystore* and *deploy/ssl*
directories.

Then run:  
``
docker compose up --wait
``

### CESNET S3

You need to set up your own [VO](https://du.cesnet.cz/cs/vlastni_vo/start) - a virtual organization. To set it up,
contact CESNET support at support@cesnet.cz, specify the required storage size, and designate at least two
administrators. All information for setting up and using S3 can be obtained from the
[DU CESNET website](https://du.cesnet.cz/cs/navody/start) or by asking for support. Once the VO is created, access
keys tied to the service identity are assigned to access the data on S3, either through GUI applications or
programmatically. This key access is a temporary solution as DU CESNET is preparing a system that generates user access
keys based on federation identity Gatekeeper.

### Apache Airflow

The process of deployment of an instance of Apache Airflow is described in its
[repository](https://gitlab.com/data-driven-research/airflow).

### Apache Hadoop

You need to [register for Metacenter](https://metavo.metacentrum.cz/cs/application/index.html)
with MetaCloud access set up. If your personal project quotas are sufficient for you,
there is no need to set up a group project. However, it is possible to register it
[here](https://projects.cloud.muni.cz/?code=WP30vIh6DjSni2oMR94fmr&state=30e401f5eb40965e56529aedf7d09f94).
The essential thing is to request sufficient resources according to the targeted use.
The actual Hadoop instance can then be set up according to the instructions on the
[Metacenter wiki](https://wiki.metacentrum.cz/wiki/Hadoop). In general, knowledge of
[OpenStack](https://www.openstack.org/), [Terraform](https://www.terraform.io/) and
[Kerberos](https://web.mit.edu/kerberos/) tools is required for this.

## Development

To build and deploy as a Docker image locally, you first need to fill *develop/config/application.properties* and
*develop/.env* files. For the Spring Boot profile **test**, there is no need to save and set up SP and IdP files or
any other certificate.

Then for local develoment instance run  
``
./local-deploy.sh
``

***

To build a Java jar you need to run the following:  
``
./build-release/build.sh test
``

Then in the *develop* directory, run:  
``
docker compose up --wait
``

For a production build, you need to run the following:  
``
./build-release/build.sh {target version}
``  
Note: The target versions used by our developer team are
**production** and **stage**.

The container registry is used to release
on [Docker Hub](https://hub.docker.com/r/cxiomi/ddr-web) with the hub name **cxiomi**, where the
images are uploaded for subsequent deployment. The application is developed so that the actual deployment can be done
with only a change of configuration files in the *deploy* directory. In the case of modifying the code and building an
image for your institution, you need to use your container repository.

The release can be triggered by:  
``
./build-release/release.sh {hub} {target version}
``

The application can be built and released at the same time using the following:  
``
./build-release/build-and-release.sh {hub} {target version}
``



## Authors

Jan Kočí - lead, architect  
Martin Vích Vlasák - network administrator  
Věnceslav Chumchal - developer  
Jakub Zach - developer

## License

Apache-2.0
