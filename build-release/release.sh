#!/bin/bash

HUB=$1
VERSION=$2

TARGET_FILE=DDRapp.jar
FULL_IMAGE_NAME="$HUB/ddr-web:$VERSION"

echo "$PWD"

docker build --build-arg TARGET_FILE=$TARGET_FILE -t "$FULL_IMAGE_NAME" -f build-release/Dockerfile .
docker push "$FULL_IMAGE_NAME"

rm  "$TARGET_FILE"