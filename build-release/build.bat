@echo off
set VERSION=%1
set FILE=DDRapp-%VERSION%.jar

gradlew build -Pversion="%VERSION%" && copy "build\libs\%FILE%" "DDRapp.jar"