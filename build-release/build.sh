#!/bin/bash

VERSION=$1
FILE=DDRapp-$VERSION.jar

./gradlew build -Pversion="$VERSION"
cp "build/libs/$FILE" "DDRapp.jar"
