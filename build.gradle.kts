plugins {
    java
    id("org.springframework.boot") version "3.2.3"
    id("io.spring.dependency-management") version "1.1.4"
    id("com.gorylenko.gradle-git-properties") version "2.4.1"
}

group = "cz.tul.cxi"
version = project.version
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
    maven { setUrl("https://build.shibboleth.net/nexus/content/repositories/releases/") }
}

dependencies {
    constraints {
        implementation("org.opensaml:opensaml-core:4.3.0")
        implementation("org.opensaml:opensaml-saml-api:4.3.0")
        implementation("org.opensaml:opensaml-saml-impl:4.3.0")
        implementation("ch.qos.logback:logback-classic:1.4.14")

    }
    implementation(platform("software.amazon.awssdk:bom:2.24.11"))
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.springframework.boot:spring-boot-starter-data-elasticsearch")
    implementation("org.hibernate.validator:hibernate-validator")
    implementation("org.springframework.session:spring-session-core")
    implementation("org.springframework.security:spring-security-saml2-service-provider")
    implementation("org.thymeleaf.extras:thymeleaf-extras-springsecurity6")
    implementation("io.sentry:sentry-spring-boot-starter:7.4.0")
    implementation("io.sentry:sentry-logback:7.4.0")
    implementation("com.fasterxml.jackson.module:jackson-module-jakarta-xmlbind-annotations")
    implementation("commons-io:commons-io:2.15.1")
    implementation("software.amazon.awssdk:s3")
    implementation("org.liquibase:liquibase-core")
    compileOnly("org.projectlombok:lombok")
    runtimeOnly("com.h2database:h2")
    runtimeOnly("org.postgresql:postgresql")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    annotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.10.2")
    testImplementation("org.testcontainers:elasticsearch:1.19.6")
    testImplementation("org.testcontainers:postgresql:1.19.6")
}


tasks.test {
    // onlyIf { project.version != "production" }
    onlyIf { project.version == "none" }
    useJUnitPlatform()
}

gitProperties {
    dateFormat = "dd-MM-yyyy HH:mmZ"
    dateFormatTimeZone = "CET"
}

tasks.bootBuildImage {
    builder.set("paketobuildpacks/builder-jammy-base:latest")
}
