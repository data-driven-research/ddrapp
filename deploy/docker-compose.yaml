x-logging: &default-logging
  driver: journald

services:
  web:
    depends_on:
      postgres:
        condition: service_healthy
      es01:
        condition: service_healthy
    image: cxiomi/ddr-web:production
    ports:
      - "443:443"
    volumes:
      - ./config/application.properties:/app/config/application.properties # directory to place application.properties
      - ./keystore/:/app/keystore # directory to place all certs
      - ./certs/:/app/certs # directory with ELK certs
    entrypoint:
      - java
      - -Xmx2048m
      - -Dsentry.environment=production
      - -Dspring.profiles.active=production # stage, test
      - -DPOSTGRESQL_HOST=postgres
      - -DPOSTGRESQL_PORT=${POSTGRESQL_PORT}
      - -DPOSTGRESQL_DATABASE=${POSTGRESQL_DATABASE}
      - -DPOSTGRESQL_USERNAME=${POSTGRESQL_USERNAME}
      - -DPOSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD}
      - -DSERVER_CERTIFICATE=${SERVER_CERTIFICATE}
      - -DSERVER_CERT_PASSWORD=${SERVER_CERT_PASSWORD}
      - -DSERVICE_PROVIDER_PRIVATE=${SERVICE_PROVIDER_PRIVATE}
      - -DSERVICE_PROVIDER_PUBLIC=${SERVICE_PROVIDER_PUBLIC}
      - -DIDENTITY_PROVIDER_CERT=${IDENTITY_PROVIDER_CERT}
      - -DSMTP_PASSWORD=${SMTP_PASSWORD}
      - -DAIRFLOW_BASIC_TOKEN=${AIRFLOW_BASIC_TOKEN}
      - -DCESNET_S3_ACCESS_KEY_ID=${CESNET_S3_ACCESS_KEY_ID}
      - -DCESNET_S3_SECRET_KEY=${CESNET_S3_SECRET_KEY}
      - -DELASTIC_URI=es01:${ES_PORT}
      - -DELASTIC_USERNAME=elastic
      - -DELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - -jar
      - DDRapp.jar
    healthcheck:
      test: "curl -I -k https://localhost:443"
      interval: 10s
      timeout: 10s
      retries: 12
      start_period: 20s
    logging: *default-logging
    restart: always
    pull_policy: always
    stop_grace_period: 10s


  postgres:
    image: bitnami/postgresql:15
    volumes:
      - ./postgres-init/:/docker-entrypoint-initdb.d
      - postgres-data:/bitnami/postgresql
    ports:
      - ${POSTGRESQL_PORT}:5432
    environment:
      - POSTGRESQL_POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - POSTGRESQL_USERNAME=${POSTGRESQL_USERNAME}
      - POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD}
      - POSTGRESQL_DATABASE=${POSTGRESQL_DATABASE}
    healthcheck:
      test: pg_isready -q -d $$POSTGRESQL_DATABASE -U $$POSTGRESQL_USERNAME
      interval: 10s
      timeout: 10s
      retries: 12
    logging: *default-logging
    restart: always
    pull_policy: always
    stop_grace_period: 10s

  setup:
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - ./certs/:/usr/share/elasticsearch/config/certs
    user: "0"
    command: >
      bash -c '
        if [ x${ELASTIC_PASSWORD} == x ]; then
          echo "Set the ELASTIC_PASSWORD environment variable in the .env file";
          exit 1;
        elif [ x${KIBANA_PASSWORD} == x ]; then
          echo "Set the KIBANA_PASSWORD environment variable in the .env file";
          exit 1;
        fi;
        if [ ! -f config/certs/ca.zip ]; then
          echo "Creating CA";
          bin/elasticsearch-certutil ca --silent --pem -out config/certs/ca.zip;
          unzip config/certs/ca.zip -d config/certs;
        fi;
        if [ ! -f config/certs/certs.zip ]; then
          echo "Creating certs";
          echo -ne \
          "instances:\n"\
          "  - name: es01\n"\
          "    dns:\n"\
          "      - es01\n"\
          "      - localhost\n"\
          "      - ddr.cxi.tul.cz\n"\
          "    ip:\n"\
          "      - 127.0.0.1\n"\
          "      - 147.230.21.189\n"\
          > config/certs/instances.yml;
          bin/elasticsearch-certutil cert --silent --pem -out config/certs/certs.zip --in config/certs/instances.yml --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key;
          unzip config/certs/certs.zip -d config/certs;
        fi;
        echo "Setting file permissions"
        chown -R root:root config/certs;
        find . -type d -exec chmod 750 \{\} \;;
        find . -type f -exec chmod 640 \{\} \;;
        echo "Waiting for Elasticsearch availability";
        until curl -s --cacert config/certs/ca/ca.crt https://es01:9200 | grep -q "missing authentication credentials"; do sleep 30; done;
        echo "Setting kibana_system password";
        until curl -s -X POST --cacert config/certs/ca/ca.crt -u "elastic:${ELASTIC_PASSWORD}" -H "Content-Type: application/json" https://es01:9200/_security/user/kibana_system/_password -d "{\"password\":\"${KIBANA_PASSWORD}\"}" | grep -q "^{}"; do sleep 10; done;
        echo "All done!";
      '

  es01:
    image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
    volumes:
      - ./certs/:/usr/share/elasticsearch/config/certs
      - es-data:/usr/share/elasticsearch/data
    ports:
      - ${ES_PORT}:9200
    environment:
      - discovery.type=single-node
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - bootstrap.memory_lock=true
      - ES_JAVA_OPTS=-Xmx3g
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=true
      - xpack.security.http.ssl.key=certs/es01/es01.key
      - xpack.security.http.ssl.certificate=certs/es01/es01.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/es01/es01.key
      - xpack.security.transport.ssl.certificate=certs/es01/es01.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=none
      - xpack.license.self_generated.type=${LICENSE}
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test: curl -I -k --user elastic:$$ELASTIC_PASSWORD https://localhost:9200
      interval: 10s
      timeout: 10s
      retries: 12
    logging: *default-logging
    restart: always
    pull_policy: always
    stop_grace_period: 10s

  kibana:
    depends_on:
      es01:
        condition: service_healthy
    image: docker.elastic.co/kibana/kibana:${STACK_VERSION}
    volumes:
      - ./config/kibana.yml:/usr/share/kibana/config/kibana.yml
      - ./ssl:/etc/kibana/config/certs
      - ./certs/:/usr/share/kibana/config/certs
      - kibana-data:/usr/share/kibana/data
    ports:
      - ${KIBANA_PORT}:5601
    environment:
      - ELASTICSEARCH_HOSTS=https://es01:9200
      - ELASTICSEARCH_USERNAME=kibana_system
      - ELASTICSEARCH_PASSWORD=${KIBANA_PASSWORD}
      - ELASTICSEARCH_SSL_CERTIFICATEAUTHORITIES=config/certs/ca/ca.crt
      - KEY_PASSPHRASE=${KEY_PASSPHRASE}
    healthcheck:
      test: curl -I -k https://localhost:5601
      interval: 10s
      timeout: 10s
      retries: 12
    logging: *default-logging
    restart: always
    pull_policy: always
    stop_grace_period: 10s

volumes:
  es-data:
    driver: local
  kibana-data:
    driver: local
  postgres-data:
    driver: local
