CREATE TABLE "users"
(
    "email"      text PRIMARY KEY,
    "department" text DEFAULT 'GUEST',
    "role"       text DEFAULT 'GUEST'
);

CREATE TABLE "resources"
(
    "id"              bigint PRIMARY KEY,
    "name_cz"         text NOT NULL,
    "name_en"         text NOT NULL,
    "internal_number" text NOT NULL,
    "activity"        text NOT NULL,
    "department"      text,
    "program"         text,
    "acronym"         text NOT NULL,
    "type"            text NOT NULL,
    "data_path"       text UNIQUE
);

CREATE TABLE "user_dataset"
(
    "user_email"   text,
    "dataset_id"   uuid,
    "access_level" text DEFAULT 'R',
    PRIMARY KEY ("user_email", "dataset_id")
);

CREATE TABLE "datasets"
(
    "id"          uuid   PRIMARY KEY,
    "name_cz"     text      NOT NULL,
    "name_en"     text      NOT NULL,
    "acronym"     text      NOT NULL,
    "resource_id" bigint    NOT NULL,
    "start_ts"    timestamp NOT NULL,
    "end_ts"      timestamp,
    "data_path"   text UNIQUE
);

CREATE TABLE "measurements"
(
    "id"              uuid,
    "repetition"      int,
    "acronym"         text      NOT NULL,
    "user_email"      text      NOT NULL,
    "dataset_id"      uuid      NOT NULL,
    "instrument_id"   text      NOT NULL,
    "start_ts"        timestamp NOT NULL,
    "end_ts"          timestamp,
    "data_path"       text,
    "uploaded"        boolean,
    "custom_metadata" jsonb DEFAULT '{}',
    PRIMARY KEY ("id", "repetition")
);

CREATE TABLE "user_instrument"
(
    "user_email"    text,
    "instrument_id" text,
    "access_level"  text DEFAULT 'R',
    PRIMARY KEY ("user_email", "instrument_id")
);

CREATE TABLE "instruments"
(
    "id"                text PRIMARY KEY,
    "name_cz"           text        NOT NULL,
    "name_en"           text        NOT NULL,
    "acronym"           text        NOT NULL,
    "description_cz"    text,
    "description_en"    text,
    "image_url"         text UNIQUE NOT NULL,
    "measuring"         boolean     NOT NULL,
    "installed"         boolean     NOT NULL,
    "department"        text        NOT NULL,
    "registration_info" jsonb       NOT NULL
);

ALTER TABLE "user_dataset"
    ADD FOREIGN KEY ("user_email") REFERENCES "users" ("email");

ALTER TABLE "user_dataset"
    ADD FOREIGN KEY ("dataset_id") REFERENCES "datasets" ("id");

ALTER TABLE "datasets"
    ADD FOREIGN KEY ("resource_id") REFERENCES "resources" ("id");

ALTER TABLE "measurements"
    ADD FOREIGN KEY ("user_email") REFERENCES "users" ("email");

ALTER TABLE "measurements"
    ADD FOREIGN KEY ("dataset_id") REFERENCES "datasets" ("id");

ALTER TABLE "measurements"
    ADD FOREIGN KEY ("instrument_id") REFERENCES "instruments" ("id");

ALTER TABLE "user_instrument"
    ADD FOREIGN KEY ("user_email") REFERENCES "users" ("email");

ALTER TABLE "user_instrument"
    ADD FOREIGN KEY ("instrument_id") REFERENCES "instruments" ("id");
